//
//  StitchSDKBridge.mm
//  Retail_REA
//
//  Created by clobotics_ccp on 2019/3/29.
//  Copyright © 2019 ice.hu. All rights reserved.
//

#import "StitchSDKBridge.h"
#import <avif.h>
#import <librav1e/rav1e.h>
#import <libwebp/encode.h>
#import <libwebp/decode.h>
#import "test.hpp"



@implementation StitchSDKBridge
{
    int currentFrameIndex;
    BOOL waitHandle;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        currentFrameIndex = 0;
        waitHandle = false;
    }
    return self;
}

- (void)handleSampleBuffer:(CMSampleBufferRef)sampleBuffer {
    
}

- (void)handleSampleBuffer:(CVImageBufferRef)sampleBuffer isUnitImage:(int)isUnitImage {

}

- (NSData *)webpEncode:(uint8_t *)bufferRef size: (CGSize)size quality: (float)quality {
    NSLog(@"webPEncode start");
    uint8_t *output = (uint8_t *) malloc(size.width * size.height * 4);
    size_t dataSize = WebPEncodeRGBA(bufferRef, size.width, size.height, size.width * 4, quality, &output);
    if (dataSize) {
        NSData * webpData = [NSData dataWithBytes:output length:dataSize];
        WebPFree(output);
        NSLog(@"webPEncode success");
        return webpData;
    }
    return nil;
}
//- (UIImage *)webpDecodeRGBA:(NSData *)data size: (CGSize)size  {
//    const uint8_t *input = (const uint8_t*)[data bytes];
//    const uint8_t *output = WebPDecodeRGBA(input, data.length, size.width, size.height)
//    
//}

- (UIImage *)webpDecode:(NSData *)data {
    NSLog(@"webPDecode start");
//    WebPData webpData;
//    WebPDataInit(&webpData);
//    webpData.bytes = data.bytes;
//    webpData.size = data.length;

    // 初始化Config，用于存储图像的Bitmap，大小等信息
    WebPDecoderConfig config;
    if (!WebPInitDecoderConfig(&config)) {
        return nil;
    }
    const uint8_t *input = (const uint8_t*)[data bytes];
    // 判断是否能够解码
    if (WebPGetFeatures(input, data.length, &config.input) != VP8_STATUS_OK) {
        return nil;
    }

    // 设定输出的Bitmap色彩空间，有Alpha通道选择RGBA，没有选择RGB
    bool has_alpha = config.input.has_alpha;
    config.output.colorspace = MODE_rgbA;
    config.options.use_threads = 1;

    // 真正开始解码，输出RGBA数据到Config的output中
    if (WebPDecode(input, data.length, &config) != VP8_STATUS_OK) {
        return nil;
    }

    // 获取图像的大小
    int width = config.input.width;
    int height = config.input.height;
    if (config.options.use_scaling) {
        width = config.options.scaled_width;
        height = config.options.scaled_height;
    }

    //RGBA矢量和对应的大小
    uint8_t *rgba = config.output.u.RGBA.rgba;
    size_t rgbaSize = config.output.u.RGBA.size;
    NSLog(@"webPDecode success");
    
    
    CGContextRef gtx = CGBitmapContextCreate(&rgba[0],
                                             width, height,
                                             8, static_cast<size_t>(width * 4),
                                             CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedLast);

    // create the image:
    CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
    UIImage * uiimage = [[UIImage alloc] initWithCGImage:toCGImage];
    
    return uiimage;
    
}


- (void)handleTest:(uint8_t *)bufferRef size: (CGSize)size fileName: (NSString *)name  {
    NSLog(@"encode start");
    printf("size: %f %f \n", size.width, size.height);
    //    avifImage image;
    //    image.height = (int)CVPixelBufferGetHeight(sampleBuffer);
    //    image.width = (int)CVPixelBufferGetWidth(sampleBuffer);
        NSString *filepath = [NSString stringWithFormat:@"%@/Documents/%@.avif", NSHomeDirectory(), name];
        const char * outputFilename = [filepath cStringUsingEncoding:[NSString defaultCStringEncoding]];
        avifBool encodeYUVDirectly = AVIF_FALSE;
        avifEncoder * encoder = NULL;
        avifRWData avifOutput = AVIF_DATA_EMPTY;
        avifRGBImage rgb;
        memset(&rgb, 0, sizeof(rgb));
        
        avifResult addImageResult;
        avifResult finishResult;
        FILE * f;
        size_t bytesWritten;

        avifImage * image = avifImageCreate(size.width, size.height, 8, AVIF_PIXEL_FORMAT_YUV444); // these values dictate what goes into the final AVIF
        // Configure image here: (see avif/avif.h)
        // * colorPrimaries
        // * transferCharacteristics
        // * matrixCoefficients
        // * avifImageSetProfileICC()
        // * avifImageSetMetadataExif()
        // * avifImageSetMetadataXMP()
        // * yuvRange
        // * alphaRange
        // * transforms (transformFlags, pasp, clap, irot, imir)

        if (encodeYUVDirectly) {
            // If you have YUV(A) data you want to encode, use this path
            printf("Encoding raw YUVA data\n");

            avifImageAllocatePlanes(image, AVIF_PLANES_YUV | AVIF_PLANES_A);

            // Fill your YUV(A) data here
            memset(image->yuvPlanes[AVIF_CHAN_Y], 255, image->yuvRowBytes[AVIF_CHAN_Y] * image->height);
            memset(image->yuvPlanes[AVIF_CHAN_U], 128, image->yuvRowBytes[AVIF_CHAN_U] * image->height);
            memset(image->yuvPlanes[AVIF_CHAN_V], 128, image->yuvRowBytes[AVIF_CHAN_V] * image->height);
            memset(image->alphaPlane, 255, image->alphaRowBytes * image->height);
        } else {
            // If you have RGB(A) data you want to encode, use this path
            printf("Encoding from converted RGBA\n");

            avifRGBImageSetDefaults(&rgb, image);
            // Override RGB(A)->YUV(A) defaults here: depth, format, chromaUpsampling, ignoreAlpha, libYUVUsage, etc

            // Alternative: set rgb.pixels and rgb.rowBytes yourself, which should match your chosen rgb.format
            // Be sure to use uint16_t* instead of uint8_t* for rgb.pixels/rgb.rowBytes if (rgb.depth > 8)
            avifRGBImageAllocatePixels(&rgb);

            // Fill your RGB(A) data here
//            memset(rgb.pixels, 255, rgb.rowBytes * image->height);
            rgb.pixels = bufferRef;
            
            avifResult convertResult = avifImageRGBToYUV(image, &rgb);
            if (convertResult != AVIF_RESULT_OK) {
                fprintf(stderr, "Failed to convert to YUV(A): %s\n", avifResultToString(convertResult));
                goto cleanup;
            }
        }

        encoder = avifEncoderCreate();
    encoder->speed = AVIF_SPEED_FASTEST;
    encoder->maxThreads = 10;
    encoder->maxQuantizer = AVIF_QUANTIZER_WORST_QUALITY;
    encoder->maxQuantizerAlpha = AVIF_QUANTIZER_WORST_QUALITY;
        // Configure your encoder here (see avif/avif.h):
        // * maxThreads
        // * minQuantizer
        // * maxQuantizer
        // * minQuantizerAlpha
        // * maxQuantizerAlpha
        // * tileRowsLog2
        // * tileColsLog2
        // * speed
        // * keyframeInterval
        // * timescale

        // Call avifEncoderAddImage() for each image in your sequence
        // Only set AVIF_ADD_IMAGE_FLAG_SINGLE if you're not encoding a sequence
        addImageResult = avifEncoderAddImage(encoder, image, 1, AVIF_ADD_IMAGE_FLAG_SINGLE);
        if (addImageResult != AVIF_RESULT_OK) {
            fprintf(stderr, "Failed to add image to encoder: %s\n", avifResultToString(addImageResult));
            goto cleanup;
        }

        finishResult = avifEncoderFinish(encoder, &avifOutput);
        if (finishResult != AVIF_RESULT_OK) {
            fprintf(stderr, "Failed to finish encode: %s\n", avifResultToString(finishResult));
            goto cleanup;
        }
        
        printf("Encode success: %zu total bytes \n", avifOutput.size);
    NSLog(@"encode success");

        f = fopen(outputFilename, "wb");
        bytesWritten = fwrite(avifOutput.data, 1, avifOutput.size, f);
        fclose(f);
        if (bytesWritten != avifOutput.size) {
            fprintf(stderr, "Failed to write %zu bytes\n", avifOutput.size);
            goto cleanup;
        }
        printf("Wrote: %s\n", outputFilename);
        
    cleanup:
        if (image) {
            avifImageDestroy(image);
        }
        if (encoder) {
            avifEncoderDestroy(encoder);
        }
        avifRWDataFree(&avifOutput);
        avifRGBImageFreePixels(&rgb); // Only use in conjunction with avifRGBImageAllocatePixels()
        
}

+ (unsigned char *)getRGBAWithImage:(UIImage *)image
{
    
//    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(image.CGImage);
    CGImageAlphaInfo alphaInfo = kCGImageAlphaPremultipliedLast;
    CGColorSpaceRef colorRef = CGColorSpaceCreateDeviceRGB();

    float width = image.size.width;
    float height = image.size.height;

    // Get source image data
    uint8_t *imageData = (uint8_t *) malloc(width * height * 4);

    CGContextRef imageContext = CGBitmapContextCreate(imageData,
            width, height,
            8, static_cast<size_t>(width * 4),
            colorRef, alphaInfo);

    CGContextDrawImage(imageContext, CGRectMake(0, 0, width, height), image.CGImage);
    CGContextRelease(imageContext);
    CGColorSpaceRelease(colorRef);
    
    // 验证由RGBA转UIimage
//    CGContextRef gtx = CGBitmapContextCreate(&imageData[0],
//                                             width, height,
//                                             8, static_cast<size_t>(width * 4),
//                                             colorRef, alphaInfo);
//
//    // create the image:
//    CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
//    UIImage * uiimage = [[UIImage alloc] initWithCGImage:toCGImage];
    
    return  imageData;
}

- (UIImage *)getImageWithRGBA: (uint8_t *)data {
    // 通过RGBA数组，创建一个DataProvider。最后一个参数是一个函数指针，用来在创建完成后清理内存用的
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, data, 720 * 1280 * 4, FreeImageData);
    // 目标色彩空间，我们这里用的就是RGBA
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    // Bitmap数据，如果含有Alpha通道，设置Premultiplied Alpha。没有就忽略Alpha通道
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast;
    // 通道数，有alpha就是4，没有就是3
    size_t components = 4;
    // 这个是用来做色彩空间变换的指示，如果超出色彩空间，比如P3转RGBA，默认会进行兼容转换
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    // 每行字节数（RGBA数组就是连续排列的多维数组，一行就是宽度*通道数），又叫做stride，因为Bitmap本质就是Pixel(uint_8)的二维数组，需要知道何时分行
    size_t bytesPerRow = components * 720;
    // 创建CGImage，参数分别意义为：宽度，高度，每通道的Bit数（RGBA自然是256，对应8Bit），每行字节数，色彩空间，Bitmap信息，数据Provider，解码数组（这个传NULL即可，其他值的话，会将经过变换比如premultiplied-alpha之后的Bitmap写回这个数组），是否过滤插值（这个一般不用开，可以在专门的图像锐化里面搞），色彩空间变换指示
    CGImageRef imageRef = CGImageCreate(720, 1280, 8, components * 8, bytesPerRow, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    // 别忘了清理DataProvider，此时会调用之前传入的清理函数
    CGDataProviderRelease(provider);
    
    UIImage *image = [UIImage imageWithCGImage: imageRef scale: 1 orientation: UIImageOrientationUp];
    return image;
}

- (void)testClicked {
    test();
}

static void FreeImageData(void *info, const void *data, size_t size) {
    free((void *)data);
}
//Tools
//
//- (UIImage *) imageFromSampleBuffer:(CVImageBufferRef) sampleBuffer
//{
//    // Get a CMSampleBuffer's Core Video image buffer for the media data
//    CVImageBufferRef imageBuffer = sampleBuffer;
//    // Lock the base address of the pixel buffer
//    CVPixelBufferLockBaseAddress(imageBuffer, 0);
//
//    // Get the number of bytes per row for the pixel buffer
//    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
//
//    // Get the number of bytes per row for the pixel buffer
//    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
//    // Get the pixel buffer width and height
//    size_t width = CVPixelBufferGetWidth(imageBuffer);
//    size_t height = CVPixelBufferGetHeight(imageBuffer);
//
//    // Create a device-dependent RGB color space
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//
//    // Create a bitmap graphics context with the sample buffer data
//    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
//                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
//    // Create a Quartz image from the pixel data in the bitmap graphics context
//    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
//    // Unlock the pixel buffer
//    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
//
//    // Free up the context and color space
//    CGContextRelease(context);
//    CGColorSpaceRelease(colorSpace);
//
//    // Create an image object from the Quartz image
//    //UIImage *image = [UIImage imageWithCGImage:quartzImage];
//    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:1.0f orientation:UIImageOrientationUp];
//
//    // Release the Quartz image
//    CGImageRelease(quartzImage);
//
//    return image;
//
//}
//
//-(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat
//{
//    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
//    CGColorSpaceRef colorSpace;
//
//    if (cvMat.elemSize() == 1) {
//        colorSpace = CGColorSpaceCreateDeviceGray();
//    } else {
//        colorSpace = CGColorSpaceCreateDeviceRGB();
//    }
//
//    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
//
//    // Creating CGImage from cv::Mat
//    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
//                                        cvMat.rows,                                 //height
//                                        8,                                          //bits per component
//                                        8 * cvMat.elemSize(),                       //bits per pixel
//                                        cvMat.step[0],                            //bytesPerRow
//                                        colorSpace,                                 //colorspace
//                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
//                                        provider,                                   //CGDataProviderRef
//                                        NULL,                                       //decode
//                                        false,                                      //should interpolate
//                                        kCGRenderingIntentDefault                   //intent
//                                        );
//
//
//    // Getting UIImage from CGImage
//    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);
//    CGDataProviderRelease(provider);
//    CGColorSpaceRelease(colorSpace);
//
//    return finalImage;
//}

@end
