//
//  ComponentViewController.swift
//  swift-test
//
//  Created by clmd on 2021/2/23.
//  Copyright © 2021 clmd铭. All rights reserved.
//

import UIKit


class ComponentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func multipleChoiceClicked(_ sender: UIButton) {
        MultipleChoiceView(options: ["选项1","选项2","选项3","选项4" ], allowMultiple: sender.tag == 1, callBack: { indexs in
            print(indexs)
            MobClick.event("optionsClicked")
        }).showin(UIApplication.shared.keyWindow!)
        
        let dic = [
            "user":"666",
            "plan ID": "2222",
            "Taskid": "33",
            "catalog": "444",
            "scene": "555",
            "model type": "2",
            "ignore" : (sender.tag == 1) ? "1" : "0"
        ]
        MobClick.event("pushVC", attributes: dic)
        MobClick.event("optionsClicked", attributes: dic)
    }

}
