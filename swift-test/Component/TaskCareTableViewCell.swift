//
//  TaskCareTableViewCell.swift
//  swift-test
//
//  Created by clmd on 2020/11/10.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class TaskCareTableViewCell: UITableViewCell {
    
    @IBOutlet weak var optionBTN: UIButton!
    var clickCallback: ()->Void = {}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        optionBTN.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
        optionBTN.tintColor = .white
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func optionClicked(_ sender: Any) {
        clickCallback()
    }
    
}
