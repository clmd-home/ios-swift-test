//
//  TaskTitleHeaderView.swift
//  swift-test
//
//  Created by clmd on 2020/11/12.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class TaskTitleHeaderView: UICollectionReusableView {

    @IBOutlet weak var titleLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setContent(title: String)  {
        titleLB.text = title
    }
}
