//
//  MultipleChoiceView.swift
//  swift-test
//
//  Created by clmd on 2021/2/23.
//  Copyright © 2021 clmd铭. All rights reserved.
//

import UIKit

class MultipleChoiceView: UIView {
    
    var callBack: ([Int])->Void = {_ in}
    
    private var selectedIndexs: [Int] = []
    private var isAllowMultiple: Bool = false
    private var optionDatasource : [String] = []
    
    let optionTBV = UITableView()
    let titleLB = UILabel()
    
    let rowHeight = 59
    
    convenience init(frame: CGRect = .zero, options: [String], allowMultiple: Bool = false, callBack: @escaping ([Int])->Void) {
        self.init(frame: frame)
        optionDatasource = options
        isAllowMultiple = allowMultiple
        self.callBack = callBack
        setup()
    }
    
    func showin(_ superview: UIView) {
        superview.addSubview(self)
        self.snp.makeConstraints({$0.edges.equalToSuperview()})
    }
    
    override func draw(_ rect: CGRect) {
        titleLB.setCornersRadius(radius: 27, roundingCorners: [.topLeft, .topRight])
    }
    
    private func setup() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let downV = UIView()
        downV.backgroundColor = .white
        self.addSubview(downV)
        downV.snp.makeConstraints({
            $0.bottom.right.left.equalToSuperview()
            $0.top.equalTo(self.safeAreaLayoutGuide.snp.bottom)
        })

        optionTBV.allowsMultipleSelection = isAllowMultiple
        optionTBV.isUserInteractionEnabled = true 
        optionTBV.register(UITableViewCell.self, forCellReuseIdentifier: "optionCellID")
        optionTBV.delegate = self
        optionTBV.dataSource = self
        self.addSubview(optionTBV)
        optionTBV.snp.makeConstraints({
            $0.left.right.equalToSuperview()
            $0.bottom.equalTo(downV.snp.top)
            $0.height.equalTo(optionDatasource.count * rowHeight)
        })
        
        
        titleLB.backgroundColor = .white
        titleLB.textAlignment = .center
        titleLB.text = isAllowMultiple ? "多选题" : "单选题"
        titleLB.font = .systemFont(ofSize: 18)
        titleLB.isUserInteractionEnabled = true
        self.addSubview(titleLB)
        titleLB.snp.makeConstraints({
            $0.right.left.equalToSuperview()
            $0.bottom.equalTo(optionTBV.snp.top)
            $0.height.equalTo(71)
        })
        let lineLB = UILabel()
        lineLB.backgroundColor = UIColor(hexString: "#B6B6B6")
        titleLB.addSubview(lineLB)
        lineLB.snp.makeConstraints({
            $0.left.right.bottom.equalToSuperview()
            $0.height.equalTo(0.5)
        })

        if isAllowMultiple {
            let confirmBTN = UIButton()
            confirmBTN.setTitle("确认", for: .normal)
            confirmBTN.titleLabel?.font = .systemFont(ofSize: 14)
            confirmBTN.setTitleColor(UIColor(hexString: "#3F51B5"), for: .normal)
            confirmBTN.addTouchUpInSideBtnAction {[weak self] btn in
                guard let `self` = self else { return }
                self.callBack(self.optionTBV.indexPathsForSelectedRows?.map({$0.row}) ?? [])
                self.removeFromSuperview()
            }
            titleLB.addSubview(confirmBTN)
            confirmBTN.snp.makeConstraints({
                $0.top.bottom.right.equalToSuperview()
                $0.width.equalTo(80)
            })
        }
        let cancelBTN = UIButton()
        cancelBTN.setTitle("取消", for: .normal)
        cancelBTN.titleLabel?.font = .systemFont(ofSize: 14)
        cancelBTN.setTitleColor(UIColor(hexString: "#3F51B5"), for: .normal)
        cancelBTN.addTouchUpInSideBtnAction {_ in
            self.removeFromSuperview()
        }
        titleLB.addSubview(cancelBTN)
        cancelBTN.snp.makeConstraints({
            $0.top.bottom.left.equalToSuperview()
            $0.width.equalTo(80)
        })
        
        
        let hideClickView = UIView()
        hideClickView.addGesture(.tap) { (_) in
            self.removeFromSuperview()
        }
        self.addSubview(hideClickView)
        hideClickView.snp.makeConstraints({
            $0.top.left.right.equalToSuperview()
            $0.bottom.equalTo(titleLB.snp.top)
        })
        
    }
    
}

extension MultipleChoiceView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionDatasource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(rowHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "optionCellID") {
            cell.textLabel?.text = optionDatasource[indexPath.row]
            cell.textLabel?.font = .systemFont(ofSize: 14)
            cell.imageView?.image = #imageLiteral(resourceName: "ARAddPoint")
            cell.selectionStyle = isAllowMultiple ? .none : .default
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.imageView?.image = #imageLiteral(resourceName: "check-square")
        
        if !isAllowMultiple {
            self.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.callBack([indexPath.row])
                self.removeFromSuperview()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard isAllowMultiple else {
            return
        }
        let cell = tableView.cellForRow(at: indexPath)
        cell?.imageView?.image = #imageLiteral(resourceName: "ARAddPoint")
    }
    
}
