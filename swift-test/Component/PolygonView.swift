//
//  PolygonView.swift
//  swift-test
//
//  Created by clmd on 2020/5/13.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class PolygonView: UIView {

    var pointList:[Double] = [0,0,0,0,0,0,0,0]
    var fillColor: UIColor = .clear
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        initViewFromNib()
        self.frame = UIScreen.main.bounds
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        initViewFromNib()
    }
    
    private func initViewFromNib() {
        let nibView = Bundle.main.loadNibNamed("PolygonView", owner: self, options: nil)?[0] as! UIView
        nibView.frame = bounds
        self.addSubview(nibView)
    }
    
    func changeTo(potnts: [Double], color: UIColor) {
        pointList = potnts
        fillColor = color
        self.setNeedsDisplay()
        
    }
    
    override func draw(_ rect: CGRect) {
        
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(0)
        context?.setFillColor(fillColor.cgColor)
        context?.beginPath()
        context?.move(to: CGPoint(x: pointList[0], y: pointList[1]))
        context?.addLine(to: CGPoint(x: pointList[2], y: pointList[3]))
        context?.addLine(to: CGPoint(x: pointList[4], y: pointList[5]))
        context?.addLine(to: CGPoint(x: pointList[6], y: pointList[7]))
        context?.drawPath(using: .fillStroke)
        context?.strokePath()
        
        
    }
    
    func transferXY(_ w: Int32, _ h: Int32, _ points: [Int32]) -> [Double] {
        let mutilW = Double(self.frame.width) / Double(w)
        let mutilH = Double(self.frame.height) / Double(h)
        var isX = true
        return points.map { (pt) -> Double in
            defer { isX = !isX}
            if isX { return Double(pt) * mutilW }
            return Double(pt) * mutilH
        }
    }
}
