//
//  ImageZoom.swift
//  Retail_REA
//
//  Created by clobotics_ccp on 2019/3/26.
//  Copyright © 2019 ice.hu. All rights reserved.
//

import UIKit

class ImageZoom: UIScrollView {
    private var image: UIImage?
    private var imageView = UIImageView()
    
    var callBack: (Int)->Void = {_ in}
    
    private func setupImageView() {
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        self.addSubview(imageView)
        let imgSize = image?.size ?? CGSize()
        imageView.size = CGSize(width: self.width, height: imgSize.height * self.width / imgSize.width)
//        imageView.center = CGPoint(x: imageView.size.width / 2, y: self.contentSize.height / 2)
        keepCenter()
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        keepCenter()
    }
    
    private func setupScroll() {
        self.bouncesZoom = true
        self.bounces = false
        self.minimumZoomScale = 0.8
        self.maximumZoomScale = 3.0
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.delegate = self
    }
    
    private static func create(_ image: UIImage?) -> ImageZoom {
        let zoom = ImageZoom(frame: .zero)
        zoom.image = image
        return zoom
    }
    
    @discardableResult
    static func show(image: UIImage?, in sup: UIView, frame: CGRect? = nil) -> ImageZoom {
        let zoom = create(image)
        sup.insertSubview(zoom, at: 0)
        if let frame = frame {
            zoom.frame = frame
        } else {
            zoom.frame = CGRect(x: 20, y: 30, width: sup.width - 40, height: sup.height - 60)
        }
        zoom.setupScroll()
        let imgSize = image?.size ?? CGSize()
        zoom.contentSize = CGSize(width: zoom.width, height: imgSize.height * zoom.width / imgSize.width)
        zoom.setupImageView()
        
        return zoom
    }
    
    static func show(images: [UIImage], in sup: UIView, frame: CGRect? = nil) -> ImageZoom {
        let zoom = create(nil)
        sup.addSubview(zoom)
        if let frame = frame {
            zoom.frame = frame
        } else {
            zoom.frame = CGRect(x: 0, y: 0, width: sup.width, height: sup.height)
        }
        zoom.setupScroll()
        zoom.loadImage(images: images)
        return zoom
    }
    func loadImage(images: [UIImage]){
        self.imageView.removeAllChildView()
        
        var nextHeight: CGFloat = 0
        for (index, img) in images.enumerated() {
            let imgSize = img.size
            let imgV = UIImageView()
            imgV.contentMode = .scaleAspectFill
            imgV.image = img
            imgV.size = CGSize(width: self.width, height: imgSize.height * self.width / imgSize.width)
            if img == #imageLiteral(resourceName: "failedtoload") {
                self.imageView.isUserInteractionEnabled = true
                imgV.isUserInteractionEnabled = true
                
                let reloadBtn = UIButton()
                reloadBtn.setBackgroundColor(.white, for: .normal)
                reloadBtn.setTitle("Reload", for: .normal)
                reloadBtn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
                reloadBtn.setTitleColor(.red, for: .normal)
                reloadBtn.tag = index + 1
                reloadBtn.addTarget(zoom, action: #selector(reloadCliced(_:)), for: .touchUpInside)
//                reloadBtn.addshadow(opacity: 0.15, color: UIColor(hexString: "27347D").cgColor)
                
                imgV.addSubview(reloadBtn)
                reloadBtn.snp.makeConstraints { (make) in
                    make.centerX.equalToSuperview()
                    make.centerY.equalToSuperview().offset(100)
                    make.width.equalTo(169)
                    make.height.equalTo(42)
                }
            }
            self.imageView.addSubview(imgV)
            imgV.center = CGPoint(x: imgV.size.width / 2, y: nextHeight + imgV.size.height / 2)
            nextHeight += imgV.height
            if index < images.count - 1 {
                nextHeight += 10
            }
        }
        self.contentSize = CGSize(width: self.width, height: nextHeight)
        self.addSubview(self.imageView)
        self.imageView.size = self.contentSize
    }
    
    @objc func reloadCliced(_ sender: UIButton){
        sender.setTitle("Loading" + "..", for: .normal)
        callBack(sender.tag - 1)
    }
    
    func keepCenter() {
        var x = self.width / 2
        if self.width / 2 < self.contentSize.width / 2 {
            x = self.contentSize.width / 2
        }
        var y = self.height / 2
        if self.height / 2 < self.contentSize.height / 2 {
            y = self.contentSize.height / 2
        }
        imageView.center = CGPoint(x: x, y: y)
    }

}

extension ImageZoom: UIScrollViewDelegate {
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        keepCenter()
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.height > scrollView.contentSize.height { return }
//        if scrollView.contentOffset.y < 0 { return }
//        let maxContentOffsetY = imageView.size.height - abs(imageView.frame.minY) - self.height
//        if scrollView.contentOffset.y > maxContentOffsetY {
//            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x, y: maxContentOffsetY), animated: false)
//        }
//        let maxX =  scrollView.contentSize.width - scrollView.size.width
//        print(scrollView.contentOffset.x, maxX)
    }
    
}
