//
//  BaseWebViewController.swift
//  Retail_REA
//
//  Created by clobotics_ccp on 2019/3/22.
//  Copyright © 2019 ice.hu. All rights reserved.
//

import UIKit
import WebKit

class BaseWebViewController: UIViewController {


    var bridgeMethod: String {
        return "bridgeMethod"
    }
    
    lazy private var webView: WKWebView = {
        return WKWebView.init(frame: .zero, configuration: webviewConfiguration())
    }()
    
    private let loadingView = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView()
        load()
        setupLoadingView()
    }
    
    private func setupLoadingView() {
        self.view.insertSubview(loadingView, aboveSubview: webView)
        loadingView.snp.makeConstraints { (snp) in
            snp.centerX.equalTo(mainWidth / 2)
            snp.centerY.equalTo(mainHeight / 2)
        }
        loadingView.startAnimating()
        loadingView.hidesWhenStopped = true
    }
    
    private func setupWebView() {
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view.addSubview(webView)
        webView.snp.makeConstraints { $0.top.bottom.leading.trailing.equalTo(0) }
    }
    
    private func webviewConfiguration() -> WKWebViewConfiguration {
        let userContent = WKUserContentController()
        userContent.add(self, name: bridgeMethod)
        let configuration = WKWebViewConfiguration()
        if let cookieString = cookie() {
            let cookieScript = WKUserScript.init(source: cookieString, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            userContent.addUserScript(cookieScript)
        }
        configuration.userContentController = userContent
        return configuration
    }
    
    private func load() {
        guard let url = url() else { return }
        let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
        webView.load(request)
    }
    
    deinit {
        webView.configuration.userContentController.removeScriptMessageHandler(forName: bridgeMethod)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewWillAppearLog()
    }
    
    fileprivate func share() {
        UIPasteboard.general.string = url()?.absoluteString
    }
    
    //for subclass implementation
    func url() -> URL? { return nil }
    func back() { }
    func viewWillAppearLog() {}
    func cookie() -> String? {return nil}

}

fileprivate enum JsAction: String {
    case back
    case share
    case h5Back
    case h5Click
}

extension JsAction {
    func go(_ target: BaseWebViewController) {
        switch self {
        case .back:
            target.back()
        case .h5Back, .h5Click:
            break
        case .share:
            target.share()
        }
    }
}

fileprivate func alertBack(_ target: UIViewController) {
    
}

extension BaseWebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard message.name == bridgeMethod, let body = message.body as? NSDictionary else { return }
        guard let actStr = body["action"] as? String else { return }
        guard let action = JsAction(rawValue: actStr) else { return }
        
        action.go(self)
    }
}

extension BaseWebViewController: WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        loadingView.stopAnimating()
        
        alertBack(self)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingView.stopAnimating()
    }
}
