//
//  SegementedView.swift
//  swift-test
//
//  Created by clmd on 2020/11/5.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class ItemView: UIView {
    func setStyle(isNormal: Bool){
        self.backgroundColor = isNormal ? .gray : .blue
    }
}

class SegementedView: UIView {
    var spaceWidth: CGFloat = 10 // 按钮间横向间隙
    var items: [ItemView] = []
    
    var clickBack: (_ index: Int)->Void = {_ in}
    
    let scrollV = UIScrollView()
    
    var currentIndex = -1 // -1为没有任何选项被选中
    
    init(items: [ItemView]) {
        super.init(frame: .zero)
        self.items = items
        scrollV.showsHorizontalScrollIndicator = false
        
        self.addSubview(scrollV)
        setupScrollV()
    }
    override func draw(_ rect: CGRect) {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupScrollV() {
        for item in scrollV.subviews {
            item.removeFromSuperview()
        }
        
        for (index,item) in items.enumerated() {
            item.isUserInteractionEnabled = true
            item.tag = 1000 + index
            let tap = UITapGestureRecognizer(target: self, action: #selector(itemClicked(tap:)))
            item.addGestureRecognizer(tap)
            item.setStyle(isNormal: true)
            scrollV.addSubview(item)
            item.snp.makeConstraints { (maker) in
                if let lastV = scrollV.viewWithTag(1000 + index - 1) {
                    maker.left.equalTo(lastV.snp.right).offset(15)
                } else {
                    maker.left.equalToSuperview().offset(15)
                }
                maker.centerY.equalToSuperview()
                maker.height.equalTo(24)
                if index == items.count - 1 {
                    maker.right.equalToSuperview().offset(-15)
                }
            }
        }
        
        scrollV.snp.makeConstraints { $0.edges.equalToSuperview()}
    }
    
    @objc func itemClicked(tap: UITapGestureRecognizer){
        for item in items {
            item.setStyle(isNormal: true)
        }
        
        if let itemV = tap.view as? ItemView {
            if currentIndex == itemV.tag - 1000 {
                currentIndex = -1
                clickBack(currentIndex)
                return
            } else {
                currentIndex = itemV.tag - 1000
                clickBack(currentIndex)
            }
            
            var setX = scrollV.contentOffset.x
            setX = setX > itemV.frame.minX ? itemV.frame.minX : setX
            setX = setX < (itemV.frame.maxX - self.bounds.width) ? (itemV.frame.maxX - self.bounds.width) : setX
            scrollV.setContentOffset(CGPoint(x: setX, y: 0), animated: true)
            
            itemV.setStyle(isNormal: false)
        }
    }
    
    func itemWithIndex(index: Int) -> ItemView? {
        // 浅拷贝
        return scrollV.viewWithTag(1000 + index) as? ItemView
    }
}
