//
//  SenceTaskView.swift
//  swift-test
//
//  Created by clmd on 2020/11/12.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit


class SenceTaskView: UIView {
    
    var selectIndex: (_ index: IndexPath) -> Void = {_ in}
    
    lazy var collectionV: UICollectionView = {
        let layout = UICollectionViewLeftAlignedLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        layout.minimumInteritemSpacing = 60
        layout.minimumLineSpacing = 10
        let colV = UICollectionView(frame: CGRect(x: 0, y: 0, width: mainWidth, height: mainHeight), collectionViewLayout: layout)
        colV.backgroundColor = .white
        colV.register(UINib(nibName: "SenceTaskCell", bundle: nil), forCellWithReuseIdentifier: "SenceTaskCellID")
        colV.register(UINib(nibName: "TaskTitleHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TaskTitleHeaderViewID")
//        colV.register(UINib(nibName: "TaskTitleHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "TaskTitleHeaderViewID")
        colV.dataSource = self
        colV.delegate = self
        return colV
    }()
    
    static func sharedInstance() -> SenceTaskView {
        let stv = SenceTaskView()
        stv.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        // 加手势
        
        return stv
    }

    override func draw(_ rect: CGRect) {
        if self.subviews.count == 0 {
            let tapView = UIView()
            self.addSubview(tapView)
            tapView.snp.makeConstraints { (maker) in
                maker.top.bottom.left.equalToSuperview()
                maker.width.equalTo(mainWidth / 3)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction(tap:)))
            tapView.addGestureRecognizer(tap)
            
            self.addSubview(collectionV)
            collectionV.snp.makeConstraints { (make) in
                make.top.right.bottom.equalToSuperview()
                make.left.equalToSuperview().offset(mainWidth)
            }
            self.alpha = 0
            layoutIfNeeded()
            
            collectionV.snp.updateConstraints { (make) in
                make.left.equalToSuperview().offset(mainWidth / 3)
            }
            UIView.animate(withDuration: 0.2) {
                self.alpha = 1
                self.layoutIfNeeded()
            }
        }
        
    }
    
    func hidden(animate: Bool = true) {
        if !animate {
            self.removeFromSuperview()
            return
        }
        self.collectionV.snp.updateConstraints { (make) in
            make.left.equalToSuperview().offset(mainWidth)
        }
        
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0
            self.layoutIfNeeded()
        } completion: { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func tapAction(tap: UITapGestureRecognizer){
        hidden()
    }

}

extension SenceTaskView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SenceTaskCellID", for: indexPath) as! SenceTaskCell
        cell.backgroundColor = UIColor(hexString: "#EEF0FC")
        cell.titleLB.text = "\(indexPath)"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: mainWidth, height: 20)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "TaskTitleHeaderViewID", for: indexPath)
        if let headerV = header as? TaskTitleHeaderView {
            headerV.setContent(title: "sss\(indexPath.section)")
        }
        return header
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("did selected \(indexPath)")
        hidden()
        selectIndex(indexPath)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellH = 73
    
        return CGSize(width: (Int(collectionView.frame.size.width) - 3*20)/2, height: cellH)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
