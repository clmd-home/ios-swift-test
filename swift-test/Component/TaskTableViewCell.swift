//
//  TaskTableViewCell.swift
//  swift-test
//
//  Created by clmd on 2020/11/10.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    @IBOutlet weak var contentLB: UILabel!
    @IBOutlet weak var lodingImgV: UIImageView!
    @IBOutlet weak var statuLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        var imgs:[UIImage] = []
        for idx in 0...17 {
            let imgname = String(format: "加载%04d.png", idx)
            if let img = UIImage.init(named: imgname) {
                imgs.append(img)
            }
        }
        lodingImgV.animationImages = imgs
        lodingImgV.animationDuration = 0.5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loding() {
        
        lodingImgV.startAnimating()
    }
    
}
