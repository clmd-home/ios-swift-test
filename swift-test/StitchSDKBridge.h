//
//  StitchSDKBridge.h
//  Retail_REA
//
//  Created by clobotics_ccp on 2019/3/29.
//  Copyright © 2019 ice.hu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnullability-completeness"


@interface StitchSDKBridge : NSObject

- (void)handleSampleBuffer:(CVImageBufferRef)sampleBuffer isUnitImage:(int)isUnitImage;
- (void)handleSampleBuffer:(CMSampleBufferRef)sampleBuffer;
- (void)handleTest:(uint8_t *)bufferRef size: (CGSize)size fileName: (NSString *)name;
- (NSData *)webpEncode:(uint8_t *)bufferRef size: (CGSize)size quality: (float)quality;
- (UIImage *)webpDecode:(NSData *)data;

- (UIImage *)getImageWithRGBA: (uint8_t *)data;
- (void)testClicked;
+ (unsigned char *)getRGBAWithImage:(UIImage *)image;
@end
#pragma clang diagnostic pop
