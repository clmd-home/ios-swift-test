//
//  ARCameraViewController.swift
//  swift-test
//
//  Created by clmd on 2020/7/24.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit
import ARKit

class ARCameraViewController: UIViewController {
    let scnView = ARSCNView()
    @IBOutlet weak var previewV: UIImageView!
    @IBOutlet weak var AddPointBTN: UIButton!
    @IBOutlet weak var takeBTN: UIButton!
    let detectornNode = ARDetectorNode()
    
    var viewCenter = CGPoint(x: mainWidth/2, y: mainHeight/2)
    
    var rulerNode: SCNNode!
    var lastRulerIndex = 0
    
    var callBack: (_ answer: String, _ img: UIImage) -> Void = {_,_  in}
    
    var answerLenth = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = SCNScene()
        let cameraNode = SCNNode()
        let camera = SCNCamera()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 0)
        scene.rootNode.addChildNode(cameraNode)
        
        scnView.scene = scene
        scnView.delegate = self
        self.view.insertSubview(scnView, at: 0)
        scnView.frame = self.view.bounds
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        if #available(iOS 11.3, *) {
            configuration.planeDetection = [.vertical,.horizontal]
        } else {
            // Fallback on earlier versions
        }
        scnView.session.delegate = self
        scnView.session.run(configuration)
        if #available(iOS 13.0, *) {
            setupCoachingOverlay()
        } else {
            // Fallback on earlier versions
        }
        
        scnView.scene.rootNode.addChildNode(detectornNode)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    
    
    @available(iOS 13.0, *)
    func setupCoachingOverlay() {
        
        let coachingOverlay = ARCoachingOverlayView()
        // Set up coaching view
        coachingOverlay.session = scnView.session
        coachingOverlay.delegate = self
        
        coachingOverlay.translatesAutoresizingMaskIntoConstraints = false
        scnView.addSubview(coachingOverlay)
        
        NSLayoutConstraint.activate([
            coachingOverlay.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            coachingOverlay.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            coachingOverlay.widthAnchor.constraint(equalTo: view.widthAnchor),
            coachingOverlay.heightAnchor.constraint(equalTo: view.heightAnchor)
            ])
        
        coachingOverlay.activatesAutomatically = true
        
        // Most of the virtual objects in this sample require a horizontal surface,
        // therefore coach the user to find a horizontal plane.
        coachingOverlay.goal = .anyPlane
    }
    // 添加采集点
    @IBAction func addPointClicked(_ sender: Any) {
        if let lastRuler = scnView.scene.rootNode.childNode(withName: String(format: "lastRuler_%d", lastRulerIndex), recursively: true) {
            rulerNode = lastRuler
            let sphereGeometry = SCNSphere(radius: 0.003)
            sphereGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        
            let point = SCNNode(geometry: sphereGeometry)
            point.position = rulerNode.convertPosition(detectornNode.position, from: scnView.scene.rootNode)
            point.name = "endPointNode"
            rulerNode.addChildNode(point)

            rulerNode.addChildNode(getLenth(from: SCNVector3Zero, to: point.position))
            
            lastRulerIndex += 1
            rulerNode = nil
        } else {
            let sphereGeometry = SCNSphere(radius: 0.003)
            sphereGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        
            rulerNode = SCNNode(geometry: sphereGeometry)
            rulerNode.position = detectornNode.position
            rulerNode.name = String(format: "lastRuler_%d", lastRulerIndex)
            scnView.scene.rootNode.addChildNode(rulerNode)
        }
        
    }
    
    func getLenth(from: SCNVector3, to: SCNVector3) -> SCNNode{
        let length = from.distance(vector: to) * 100
        let text = String(format: "%.1f cm", length)
        let labelNode = LabelNode(position: (to - from) / 2, text: text)
        labelNode.name = "\(length)"
        return labelNode
    }
    
    // 撤销点
    @IBAction func revocationClicked(_ sender: Any) {

        if let lastRuler = scnView.scene.rootNode.childNode(withName: String(format: "lastRuler_%d", lastRulerIndex), recursively: true) {
            lastRuler.removeFromParentNode()
            rulerNode = nil
        }else if let lastRuler = scnView.scene.rootNode.childNode(withName: String(format: "lastRuler_%d", lastRulerIndex - 1), recursively: true) {
            lastRuler.removeFromParentNode()
            lastRulerIndex -= 1
        }
    }

    @IBAction func takePhotoClicked(_ sender: Any) {
        guard lastRulerIndex > 0, previewV.image == nil else { return }
        detectornNode.opacity = 0
        rulerNode?.opacity = 0
        let img = scnView.snapshot()
        detectornNode.opacity = 1
        rulerNode?.opacity = 1
        previewV.image = img
        previewV.isHidden = false
    }
    
    @IBAction func remakeClicked(_ sender: Any) {
        previewV.image = nil
        previewV.isHidden = true
    }
    
    @IBAction func confirmClicked(_ sender: Any) {
        if let lastRuler = scnView.scene.rootNode.childNode(withName: String(format: "lastRuler_%d", lastRulerIndex - 1), recursively: false), let endpoint = lastRuler.childNode(withName: "endPointNode", recursively: false) {
            let lenth = SCNVector3Zero.distance(vector: endpoint.position) * 100
            answerLenth = String(format: "%.1f cm", lenth)
        }
        guard answerLenth.count > 0, previewV.image != nil else { return}
        callBack(answerLenth, previewV.image ?? UIImage())
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gobackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ARCameraViewController: ARSessionDelegate {
    func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
        
    }
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
    }
}

extension ARCameraViewController: ARCoachingOverlayViewDelegate {
    @available(iOS 13.0, *)
    func coachingOverlayViewWillActivate(_ coachingOverlayView: ARCoachingOverlayView) {
        
    }
    @available(iOS 13.0, *)
    func coachingOverlayViewDidDeactivate(_ coachingOverlayView: ARCoachingOverlayView) {
        AddPointBTN.isEnabled = true
        takeBTN.isEnabled = true
    }
}

extension ARCameraViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        updatePlaceNodePosition()
    }
    
    private func updatePlaceNodePosition() {
        guard let frame = scnView.session.currentFrame else { return }
        let translation = matrix_identity_float4x4
        var transform:simd_float4x4!
            
            //如果检测到平面
        guard let result = scnView.hitTest(viewCenter, types: .existingPlaneUsingExtent).first else { return }
        
        transform = result.worldTransform * SIMD3<Float>(.pi/2, 0, -frame.camera.eulerAngles.z).rotationMatrix()
        //真实坐标系
        detectornNode.simdTransform = matrix_multiply(transform, translation)
        
        if let lenthstrNode = detectornNode.childNode(withName: "lenthString", recursively: false) {
            lenthstrNode.removeFromParentNode()
        }
        if rulerNode != nil {
            let endPos = rulerNode?.convertPosition(detectornNode.position, from: scnView.scene.rootNode) ?? SCNVector3Zero
                
            let lenthStr = String(format: "%.1f cm", SCNVector3Zero.distance(vector: endPos) * 100)
            let lenthNode = LabelNode(position: SCNVector3Zero, text: lenthStr)
            lenthNode.name = "lenthString"
            detectornNode.addChildNode(lenthNode)
                
            let liness = cylinderLine(from: SCNVector3Zero, to: endPos, segments: 5)
            if let linesss = rulerNode?.childNode(withName: "lineNode", recursively: true) {
                linesss.removeFromParentNode()
            }
            liness.name = "lineNode"
            rulerNode?.addChildNode(liness)
        }
        
    }

    func cylinderLine(from: SCNVector3, to: SCNVector3, segments: Int) -> SCNNode {
        
        let distance = from.distance(vector: to)
        
        let cylinder = SCNCylinder(radius: 0.001,
                                   height: CGFloat(distance))

        cylinder.radialSegmentCount = segments
        cylinder.firstMaterial?.diffuse.contents = UIColor.yellow
        
        let lineNode = SCNNode(geometry: cylinder)
        
        lineNode.position = SCNVector3(((from.x + to.x)/2),
                                       ((from.y + to.y)/2),
                                       ((from.z + to.z)/2))
        lineNode.eulerAngles = SCNVector3(Float.pi/2,
                                          acos((to.z - from.z)/distance),
                                          atan2(to.y - from.y, to.x - from.x))
        return lineNode
    }
}


//mark: ARDetectorNode

class ARDetectorNode: SCNNode {
    let placeFromSurfaceDistance:Float = 0 //放置在平面的 距离
    var rulerLenth: SCNNode = SCNNode()
    
    override init() {
        super.init()
        
        let imageNode = ARImageNode(width: 0.1, height: 0.1, alpha:0.4, image: #imageLiteral(resourceName: "detector") )
        imageNode.position = SCNVector3(0, 0, placeFromSurfaceDistance)
        imageNode.renderingOrder = 10
        self.addChildNode(imageNode)
        
        let sphereGeometry = SCNSphere(radius: 0.003)
        sphereGeometry.firstMaterial?.diffuse.contents = UIColor.white
        let dotNode = SCNNode(geometry: sphereGeometry)
        self.addChildNode(dotNode)
        
//        rulerLenth = LabelNode(position: SCNVector3Zero, text: "dalkdjf")
//        rulerLenth.opacity = 0
//        self.addChildNode(rulerLenth)
        self.opacity = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class LabelNode: SCNNode {
    init(position: SCNVector3, text: String, scale: Float = 0.001) {
        super.init()
        
        let labelGeometry = SCNText(string: text, extrusionDepth: 0)
        labelGeometry.alignmentMode = CATextLayerAlignmentMode.center.rawValue
        labelGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        self.geometry = labelGeometry
        self.scale = SCNVector3(scale, scale, scale)
        self.position = position
        self.constraints = [SCNBillboardConstraint()]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ARImageNode: SCNNode {
    
    init(width:CGFloat = 0.3 ,
         height:CGFloat = 0.3,
         alpha:CGFloat = 1,
         image:UIImage) {
        
        super.init()
        name = "ARImageNode"

        //SCNNode
        let scene = SCNPlane(width: width, height: height)
        scene.firstMaterial?.isDoubleSided = true
        scene.firstMaterial?.diffuse.contents = image.image(alpha: alpha)
        geometry = scene
        
    }
    
    func setRightAngel(){
        self.eulerAngles.y -= .pi/2
        self.eulerAngles.z -= .pi/2
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
