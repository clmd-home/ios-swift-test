//
//  WebCookieViewController.swift
//  swift-test
//
//  Created by clmd on 2021/2/19.
//  Copyright © 2021 clmd铭. All rights reserved.
//

import UIKit
import WebKit

class WebCookieViewController: BaseWebViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 缓存清理 test1
        
//        let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache])
//        let date = NSDate(timeIntervalSince1970: 0)
//        WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler: {})
    }

    override func url() -> URL? {
        return URL(string: "http://asset-prerelease.chinaeast2.cloudapp.chinacloudapi.cn/execute/device/detail?preview=1&id=7b63ff44-1400-43fb-86ae-e37a4aa60920&name=%E6%89%A9%E5%8D%9A%E5%89%8D%E8%A3%85%E7%89%883%E5%8F%B7%E9%97%A8%E5%BA%97&address=%E4%B8%8A%E6%B5%B7%E5%B8%82%E4%B8%8A%E6%B5%B7%E5%B8%82%E9%97%B5%E8%A1%8C%E5%8C%BA%E4%B8%9C%E5%B7%9D%E8%B7%AF557%E5%8F%B7&longitude=121.4603042602539&latitude=31.021757125854492&state=1")
    }
    
    override func cookie() -> String? {
        return "document.cookie = 'asset-management-token=lCBsFBOuWFMfB1dwpTBzqUap3WSVI5xfUrPBO-F00CdfhA84Rfqr8rPO827hgxof6LT_rn5-VmK0A2NHiOo6WZEBABQUqZsfw6YTtiLMR5wB7rLPTHhYuIgmEqaP3lTwImsmfL1sTf6CwemuG24VIiW40MJ6VvoOlxrQviZfCL-gS4Cf3eVU0NX90EAXual8o8GWNEhfC8s0zZ7ybfP6SOOwQ1spKxOJIimZwhsy5Q5LELi-Cwp3oXik2MAAfhBdxTVtKiaFrgckmuxB-XKmlqVN8F_9Z5cUfbdX_SHUwHenmMiQ6XIbFPSnvnO32SqXYj2UAThz0fEAPPlmYEp9Up3xuFr9-oypvSbYtItmwjVTYtPYt7dWVG0529I6ss4ui86oD8tb2R-74O7Hd86fiemAYUs5vAT-kPfN6ER4owD9SRh7CjK65zB1Up4Up1WPulnrQF-RRNtuT_yK8iihRsI5TY_QP-ye_H4BYPIN-EYYFxhjC8I2rxilVEluwvJvY0w5hNSbdm7AJzuZtmBQMPXuAky3Amf41iYF4vYXwOiRSDS_PrfLG92BiyhvycVG;domain=asset-prerelease.chinaeast2.cloudapp.chinacloudapi.cn;path=/'"
    }
    
    deinit {
        print("deinit")
    }
}
