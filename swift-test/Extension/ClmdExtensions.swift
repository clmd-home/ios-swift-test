//
//  ClmdExtensions.swift
//  swift-test
//
//  Created by clmd on 2020/7/24.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import Foundation
import UIKit
import ARKit
import Accelerate

let mainBounds = UIScreen.main.bounds
let mainSize = mainBounds.size
let mainWidth = mainSize.width
let mainHeight = mainSize.height

extension UIImage {
    func image(alpha: CGFloat) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}

extension float3 {
    func rotationMatrix() -> float4x4 {
        var matrix: float4x4 = matrix_identity_float4x4
        let x = self.x
        let y = self.y
        let z = self.z
        matrix.columns.0.x = cos(y) * cos(z)
        matrix.columns.0.y = cos(z) * sin(x) * sin(y) - cos(x) * sin(z)
        matrix.columns.0.z = cos(x) * cos(z) * sin(y) + sin(x) * sin(z)
        matrix.columns.1.x = cos(y) * sin(z)
        matrix.columns.1.y = cos(x) * cos(z) + sin(x) * sin(y) * sin(z)
        matrix.columns.1.z = -cos(z) * sin(x) + cos(x) * sin(y) * sin(z)
        matrix.columns.2.x = -sin(y)
        matrix.columns.2.y = cos(y) * sin(x)
        matrix.columns.2.z = cos(x) * cos(y)
        matrix.columns.3.w = 1.0
        return matrix
    }
}

extension SCNVector3 {
    
    func distance(vector: SCNVector3) -> Float {
        return (self - vector).length()
    }
    
    func length() -> Float {
        return sqrtf(x*x + y*y + z*z)
    }
}
func - (left: SCNVector3, right: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(left.x - right.x, left.y - right.y, left.z - right.z)
}
func / (vector: SCNVector3, scalar: Float) -> SCNVector3 {
    return SCNVector3Make(vector.x / scalar, vector.y / scalar, vector.z / scalar)
}

extension UIImage {
    
    /// 重绘图片大小
    func resizedImage(outputSize: CGSize) -> UIImage? {
        if size == outputSize {
            return self
        }else {
            UIGraphicsBeginImageContext(outputSize)
//            UIGraphicsBeginImageContextWithOptions(outputSize, false, 0.0)
            draw(in: CGRect(x: 0, y: 0, width: outputSize.width, height: outputSize.height))
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            guard let newImage = scaledImage else {
                return nil
            }
            return newImage
        }
    }
    
    /// UIImage -> CVPixelBuffer
    func imageToPixelBuffer(outputSize: CGSize) -> CVPixelBuffer? {
        
        let inputImage: UIImage = self
        var pixelBuffer: CVPixelBuffer? = nil
        guard let cgImage: CGImage = inputImage.cgImage else {
            return pixelBuffer
        }
        /// 分配内存，创建CVPixelBuffer
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(outputSize.width), Int(outputSize.height), kCVPixelFormatType_32ARGB, nil, &pixelBuffer)
        if status == kCVReturnSuccess, let pixelBuffer = pixelBuffer {
            /// 写入数据
            let bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer)
            CVPixelBufferLockBaseAddress(pixelBuffer, .readOnly)
            let context = CGContext(data: CVPixelBufferGetBaseAddress(pixelBuffer),
                                    width: Int(outputSize.width),
                                    height: Int(outputSize.height),
                                    bitsPerComponent: 8,
                                    bytesPerRow: bytesPerRow,
                                    space: CGColorSpaceCreateDeviceRGB(),
                                    bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.noneSkipFirst.rawValue)
            context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: outputSize.width, height: outputSize.height))
            CVPixelBufferUnlockBaseAddress(pixelBuffer, .readOnly)
            return pixelBuffer
        }
        return pixelBuffer
    }
    
    /// CVPixelBuffer -> UIImage
    class func pixelBufferToImage(pixelBuffer: CVPixelBuffer) -> UIImage? {
//        let type = CVPixelBufferGetPixelFormatType(pixelBuffer)
        
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer)
        
        CVPixelBufferLockBaseAddress(pixelBuffer, .readOnly)
        guard let context = CGContext(data: CVPixelBufferGetBaseAddress(pixelBuffer),
                                      width: width,
                                      height: height,
                                      bitsPerComponent: 8,
                                      bytesPerRow: bytesPerRow,
                                      space: CGColorSpaceCreateDeviceRGB(),
                                      bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.noneSkipFirst.rawValue),
            let imageRef = context.makeImage() else
        {
                return nil
        }
        
        let newImage = UIImage(cgImage: imageRef, scale: 1, orientation: UIImage.Orientation.up)
        CVPixelBufferUnlockBaseAddress(pixelBuffer, .readOnly)
        
        return newImage
    }
    
    // 截图
    func cropped(cropRect: CGRect) -> UIImage? {
        let scale = self.size.height / UIScreen.main.bounds.size.height
        
        // 有些拍摄的图片比预览的图要多一定的宽度，这个是拍摄图左侧到预览左侧的距离
        let extraSpace = (self.size.width / scale - UIScreen.main.bounds.size.width) / 2
        
        let croppedRect = CGRect(x: cropRect.origin.x * scale + extraSpace * scale, y: cropRect.origin.y * scale , width: cropRect.size.width * scale , height: cropRect.size.height * scale)
        if let cgimage = self.cgImage?.cropping(to: croppedRect){
            return UIImage(cgImage: cgimage)
        }
        return nil
    }
    
    // 将image绘制到画布上，多余部分填充
    func drawImageOnCanvas(canvasSize: CGSize, canvasColor: UIColor ) -> UIImage {

        let rect = CGRect(origin: .zero, size: canvasSize)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)

        // fill the entire image
        canvasColor.setFill()
        UIRectFill(rect)

        // calculate a Rect the size of the image to draw, centered in the canvas rect
        let centeredImageRect = CGRect(x: (canvasSize.width - self.size.width) / 2,
                                       y: (canvasSize.height - self.size.height) / 2,
                                       width: self.size.width,
                                       height: self.size.height)

        // get a drawing context
        let context = UIGraphicsGetCurrentContext();

        // "cut" a transparent rectanlge in the middle of the "canvas" image
        context?.clear(centeredImageRect)

        // draw the image into that rect
        self.draw(in: centeredImageRect)

        // get the new "image in the center of a canvas image"
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!

    }
}

extension Dictionary {
    func toJson() -> String{
        var jsonStr = ""
        if let data = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted){
            jsonStr = (String(data: data, encoding: .utf8) ?? "")
        }
        return jsonStr
    }
    
    func toString() ->String {
        var str = self.toJson().replacingOccurrences(of: "\n", with: "")
        str = str.replacingOccurrences(of: " ", with: "")
        return str + "\n"
    }
}

// 可以设置内边距的label
class HomePaddingLabel: UILabel {
    
    var textInsets: UIEdgeInsets = .zero
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insets = textInsets
        var rect = super.textRect(forBounds: bounds.inset(by: insets), limitedToNumberOfLines: numberOfLines)
        
        rect.origin.x -= insets.left
        rect.origin.y -= insets.top
        rect.size.width += (insets.left + insets.right)
        rect.size.height += (insets.top + insets.bottom)
        return rect
    }
    
}

extension UIView{
    public func addShadow(opacity:Float = 0.5,
                              shadowOffset:CGSize = .zero,
                              shadowColor:UIColor = .lightGray){
        self.layer.shadowOpacity = opacity //阴影区域透明度
        self.layer.shadowColor = shadowColor.cgColor // 阴影区域颜色
        self.layer.shadowOffset = shadowOffset //阴影区域范围
        self.layer.masksToBounds = false
    }
    
    func removeAllChildView() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    func setCornersRadius(radius: CGFloat, roundingCorners: UIRectCorner) {
        
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: roundingCorners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        maskLayer.shouldRasterize = true
        maskLayer.rasterizationScale = UIScreen.main.scale
        
        self.layer.mask = maskLayer
    }
}

public extension UIView {
    
    /// Size of view.
    var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }
    
    /// Width of view.
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    /// Height of view.
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

typealias BtnAction = (UIButton)->()
extension UIButton{

///  gei button 添加一个属性 用于记录点击tag
   private struct AssociatedKeys{
      static var actionKey = "actionKey"
   }
    
    @objc dynamic var actionDic: NSMutableDictionary? {
        set{
            objc_setAssociatedObject(self,&AssociatedKeys.actionKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        get{
            if let dic = objc_getAssociatedObject(self, &AssociatedKeys.actionKey) as? NSDictionary{
                return NSMutableDictionary.init(dictionary: dic)
            }
            return nil
        }
    }

    @objc dynamic fileprivate func DIY_button_add(action:@escaping  BtnAction ,for controlEvents: UIControl.Event) {
        let eventStr = NSString.init(string: String.init(describing: controlEvents.rawValue))
        if let actions = self.actionDic {
            actions.setObject(action, forKey: eventStr)
            self.actionDic = actions
        }else{
            self.actionDic = NSMutableDictionary.init(object: action, forKey: eventStr)
        }
        
        switch controlEvents {
            case .touchUpInside:
                self.addTarget(self, action: #selector(touchUpInSideBtnAction), for: .touchUpInside)
            case .touchUpOutside:
                self.addTarget(self, action: #selector(touchUpOutsideBtnAction), for: .touchUpOutside)
        default:
            return
        }
    }

    @objc fileprivate func touchUpInSideBtnAction(btn: UIButton) {
        if let actionDic = self.actionDic  {
            if let touchUpInSideAction = actionDic.object(forKey: String.init(describing: UIControl.Event.touchUpInside.rawValue)) as? BtnAction{
                  touchUpInSideAction(self)
            }
        }
    }

    @objc fileprivate func touchUpOutsideBtnAction(btn: UIButton) {
        if let actionDic = self.actionDic  {
            if let touchUpOutsideBtnAction = actionDic.object(forKey:   String.init(describing: UIControl.Event.touchUpOutside.rawValue)) as? BtnAction{
                touchUpOutsideBtnAction(self)
            }
        }
    }
    
    @discardableResult
    func addTouchUpInSideBtnAction(_ action:@escaping BtnAction) -> UIButton{
        self.DIY_button_add(action: action, for: .touchUpInside)
        return self
    }
    @discardableResult
    func addTouchUpOutSideBtnAction(_ action:@escaping BtnAction) -> UIButton{
        self.DIY_button_add(action: action, for: .touchUpOutside)
        return self
    }
}

extension UIButton {
    private func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        self.setBackgroundImage(image(withColor: color), for: state)
    }
    
    func addshadow(opacity: Float = 0.8, color: CGColor = UIColor.black.cgColor, size: CGSize = CGSize(width: 2, height: 2)){
        self.layer.shadowOpacity = opacity //阴影区域透明度
        self.layer.shadowColor = color // 阴影区域颜色
        self.layer.shadowOffset = size //阴影区域范围
    }
    
}

typealias viewAction = (UIGestureRecognizer)->()
//添加手势枚举
extension UIView {
    enum GestureENUM {
        case tap //点击
        case long //长按
        case pan //拖拽
        case roation //旋转
        case swipe //轻扫
        case pinch //捏合
    }
    
    private struct AssociatedKeys {
        static var actionKey = "gestureKey"
    }
    
    @objc dynamic var action:viewAction? {
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.actionKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
        get {
            if let action = objc_getAssociatedObject(self, &AssociatedKeys.actionKey) as? viewAction {
                return action
            }
            return nil
        }
    }
    
    @objc func viewTapAction(gesture: UIGestureRecognizer) {
        if action != nil {
            action!(gesture)
        }
    }
    /// 添加手势
    func addGesture( _ gesture : GestureENUM , response:@escaping viewAction) {
        
        self.isUserInteractionEnabled = true
        switch gesture {
        case .tap: //点击
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            tapGesture.numberOfTouchesRequired = 1
            tapGesture.numberOfTapsRequired = 1
            self.addGestureRecognizer(tapGesture)
            self.action = response
        case .long: //长按
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            self.addGestureRecognizer(longPress)
            self.action = response
        case .pan: //拖拽
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            self.addGestureRecognizer(panGesture)
            self.action = response
        case .roation: // 旋转
            let roation = UIRotationGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            self.addGestureRecognizer(roation)
            self.action = response
        case .swipe: //轻扫
            let swipe = UISwipeGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            self.addGestureRecognizer(swipe)
            self.action = response
        case .pinch: //捏合
            let pinch = UIPinchGestureRecognizer(target: self, action: #selector(viewTapAction(gesture:)))
            self.addGestureRecognizer(pinch)
            self.action = response
        }
    }
}

//获取指定sb指定id的vc
func getVC(withName name: String, vcId id: String) -> UIViewController {
    let sb = UIStoryboard(name: name, bundle: nil)
    return sb.instantiateViewController(withIdentifier: id)
}

func backToHomeList(){
    UIApplication.shared.keyWindow?.rootViewController = getVC(withName: "Main", vcId: "HomeListVC")
}

extension CVPixelBuffer {

  /**
   Returns thumbnail by cropping pixel buffer to biggest square and scaling the cropped image to
   model dimensions.
   */
  func cropThumbnail(ofSize size: CGSize ) -> CVPixelBuffer? {

    let imageWidth = CVPixelBufferGetWidth(self)
    let imageHeight = CVPixelBufferGetHeight(self)
    let pixelBufferType = CVPixelBufferGetPixelFormatType(self)
    assert(pixelBufferType == kCVPixelFormatType_32BGRA)

    let inputImageRowBytes = CVPixelBufferGetBytesPerRow(self)
    let imageChannels = 4
    let thumbnailSize = max(imageWidth, imageHeight)
    CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
    
    var originX = 0
    var originY = 0
    if imageWidth > imageHeight {
      originX = (imageWidth - imageHeight) / 2
    } else {
      originY = (imageHeight - imageWidth) / 2
    }
    // Finds the biggest square in the pixel buffer and advances rows based on it.
//    guard let inputBaseAddress = CVPixelBufferGetBaseAddress(self)?.advanced(
//        by: originY * inputImageRowBytes + originX * imageChannels) else {
//      return nil
//    }
    guard let inputBaseAddress = CVPixelBufferGetBaseAddress(self) else {
      return nil
    }
    // Gets vImage Buffer from input image
    var inputVImageBuffer = vImage_Buffer(
        data: inputBaseAddress, height: UInt(thumbnailSize), width: UInt(thumbnailSize),
        rowBytes: inputImageRowBytes)
    let thumbnailRowBytes = Int(size.width) * imageChannels
    guard  let thumbnailBytes = malloc(Int(size.height) * thumbnailRowBytes) else {
      return nil
    }
    // Allocates a vImage buffer for thumbnail image.
    var thumbnailVImageBuffer = vImage_Buffer(data: thumbnailBytes, height: UInt(size.height), width: UInt(size.width), rowBytes: thumbnailRowBytes)
    // Performs the scale operation on input image buffer and stores it in thumbnail image buffer.
    let scaleError = vImageScale_ARGB8888(&inputVImageBuffer, &thumbnailVImageBuffer, nil, vImage_Flags(0))
    CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
    guard scaleError == kvImageNoError else {
      return nil
    }
    let releaseCallBack: CVPixelBufferReleaseBytesCallback = {mutablePointer, pointer in
      if let pointer = pointer {
        free(UnsafeMutableRawPointer(mutating: pointer))
      }
    }
    
    let byteData = Data(bytes: thumbnailVImageBuffer.data, count: thumbnailRowBytes * Int(size.height))
    let bytes = Array<UInt8>(unsafeData: byteData)!
    var floats = [Float]()
    for i in 0..<bytes.count {
        if i % thumbnailRowBytes >= 300 * 4 {
            floats.append(1)
        } else {
            floats.append(Float(bytes[i]) / 255.0)
        }
    }
    let dddd = Data(copyingBufferOf: floats)
    dddd.withUnsafeBytes { rawBufferPointer in
        let rawPtr = rawBufferPointer.baseAddress!
        let mmm: UnsafeMutableRawPointer = UnsafeMutableRawPointer(mutating: rawPtr)
        thumbnailVImageBuffer.data = mmm
        // ... use `rawPtr` ...
    }

    var thumbnailPixelBuffer: CVPixelBuffer?
    // Converts the thumbnail vImage buffer to CVPixelBuffer
    let conversionStatus = CVPixelBufferCreateWithBytes(
        nil, Int(size.width), Int(size.height), pixelBufferType, thumbnailBytes,
        thumbnailRowBytes, releaseCallBack, nil, nil, &thumbnailPixelBuffer)
    guard conversionStatus == kCVReturnSuccess else {
      free(thumbnailBytes)
      return nil
    }
    return thumbnailPixelBuffer
  }
    
    
    public func resizePixelBuffer(_ srcPixelBuffer: CVPixelBuffer,
                                  cropX: Int,
                                  cropY: Int,
                                  cropWidth: Int,
                                  cropHeight: Int,
                                  scaleWidth: Int,
                                  scaleHeight: Int) -> CVPixelBuffer? {
        let flags = CVPixelBufferLockFlags(rawValue: 0)
        let pixelFormat = CVPixelBufferGetPixelFormatType(srcPixelBuffer)
        guard kCVReturnSuccess == CVPixelBufferLockBaseAddress(srcPixelBuffer, flags) else {
            return nil
        }
        defer { CVPixelBufferUnlockBaseAddress(srcPixelBuffer, flags) }

        guard let srcData = CVPixelBufferGetBaseAddress(srcPixelBuffer) else {
            print("Error: could not get pixel buffer base address")
            return nil
        }

        let srcHeight = CVPixelBufferGetHeight(srcPixelBuffer)
        let srcWidth = CVPixelBufferGetWidth(srcPixelBuffer)
        let srcBytesPerRow = CVPixelBufferGetBytesPerRow(srcPixelBuffer)
        let offset = cropY*srcBytesPerRow + cropX*4

        var srcBuffer: vImage_Buffer!
        var paddedSrcPixelBuffer: CVPixelBuffer!

        if (cropX < 0 || cropY < 0 || cropX + cropWidth > srcWidth || cropY + cropHeight > srcHeight) {
            let paddingLeft = abs(min(cropX, 0))
            let paddingRight = max((cropX + cropWidth) - (srcWidth - 1), 0)
            let paddingBottom = max((cropY + cropHeight) - (srcHeight - 1), 0)
            let paddingTop = abs(min(cropY, 0))

            let paddedHeight = paddingTop + srcHeight + paddingBottom
            let paddedWidth = paddingLeft + srcWidth + paddingRight

            guard kCVReturnSuccess == CVPixelBufferCreate(kCFAllocatorDefault, paddedWidth, paddedHeight, pixelFormat, nil, &paddedSrcPixelBuffer) else {
                print("failed to allocate a new padded pixel buffer")
                return nil
            }

            guard kCVReturnSuccess == CVPixelBufferLockBaseAddress(paddedSrcPixelBuffer, flags) else {
                return nil
            }

            guard let paddedSrcData = CVPixelBufferGetBaseAddress(paddedSrcPixelBuffer) else {
                print("Error: could not get padded pixel buffer base address")
                return nil
            }

            let paddedBytesPerRow = CVPixelBufferGetBytesPerRow(paddedSrcPixelBuffer)
            for yIndex in paddingTop..<srcHeight+paddingTop {
                let dstRowStart = paddedSrcData.advanced(by: yIndex*paddedBytesPerRow).advanced(by: paddingLeft*4)
                let srcRowStart = srcData.advanced(by: (yIndex - paddingTop)*srcBytesPerRow)
                dstRowStart.copyMemory(from: srcRowStart, byteCount: srcBytesPerRow)
            }

            let paddedOffset = (cropY + paddingTop)*paddedBytesPerRow + (cropX + paddingLeft)*4
            srcBuffer = vImage_Buffer(data: paddedSrcData.advanced(by: paddedOffset),
                                      height: vImagePixelCount(cropHeight),
                                      width: vImagePixelCount(cropWidth),
                                      rowBytes: paddedBytesPerRow)

        } else {
            srcBuffer = vImage_Buffer(data: srcData.advanced(by: offset),
                                      height: vImagePixelCount(cropHeight),
                                      width: vImagePixelCount(cropWidth),
                                      rowBytes: srcBytesPerRow)
        }

        let destBytesPerRow = scaleWidth*4
        guard let destData = malloc(scaleHeight*destBytesPerRow) else {
            print("Error: out of memory")
            return nil
        }
        var destBuffer = vImage_Buffer(data: destData,
                                       height: vImagePixelCount(scaleHeight),
                                       width: vImagePixelCount(scaleWidth),
                                       rowBytes: destBytesPerRow)

        let vImageFlags: vImage_Flags = vImage_Flags(kvImageEdgeExtend)
        let error = vImageScale_ARGB8888(&srcBuffer, &destBuffer, nil, vImageFlags)
        if error != kvImageNoError {
            print("Error:", error)
            free(destData)
            return nil
        }

        let releaseCallback: CVPixelBufferReleaseBytesCallback = { _, ptr in
            if let ptr = ptr {
                free(UnsafeMutableRawPointer(mutating: ptr))
            }
        }

        var dstPixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreateWithBytes(nil, scaleWidth, scaleHeight,
                                                  pixelFormat, destData,
                                                  destBytesPerRow, releaseCallback,
                                                  nil, nil, &dstPixelBuffer)
        if status != kCVReturnSuccess {
            print("Error: could not create new pixel buffer")
            free(destData)
            return nil
        }

        if paddedSrcPixelBuffer != nil {
            CVPixelBufferUnlockBaseAddress(paddedSrcPixelBuffer, flags)
        }


        return dstPixelBuffer
    }
}


extension UIColor {
     
    // Hex String -> UIColor
    convenience init(hexString: String) {
        let hexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
         
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
         
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
         
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
         
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
         
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
     
    // UIColor -> Hex String
    var hexString: String? {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
         
        let multiplier = CGFloat(255.999999)
         
        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else {
            return nil
        }
         
        if alpha == 1.0 {
            return String(
                format: "#%02lX%02lX%02lX",
                Int(red * multiplier),
                Int(green * multiplier),
                Int(blue * multiplier)
            )
        }
        else {
            return String(
                format: "#%02lX%02lX%02lX%02lX",
                Int(red * multiplier),
                Int(green * multiplier),
                Int(blue * multiplier),
                Int(alpha * multiplier)
            )
        }
    }
}




