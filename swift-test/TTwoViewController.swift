//
//  TTwoViewController.swift
//  swift-test
//
//  Created by clmd on 2020/11/20.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class TestV: ItemView {
    var name: String!
    var label: UILabel!
    var numLB: HomePaddingLabel!
    
    init(name: String = "unknown") {
        super.init(frame: CGRect.zero)
        self.name = name
        setupCommentField(num: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupCommentField(num: Int) {
        for item in self.subviews {
            item.removeFromSuperview()
        }
        
        self.backgroundColor = UIColor(hexString: "#F3F3F3")
        
        label = UILabel()
        self.addSubview(label)
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = name
        label.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-15).priority(500)
        }
        if num > 0 {
            numLB = HomePaddingLabel()
            numLB.textInsets = UIEdgeInsets(top: 1, left: 4, bottom: 1, right: 4)
            self.addSubview(numLB)
            numLB.text = "\(num)"
            numLB.font = UIFont.systemFont(ofSize: 11)
            numLB.textAlignment = .center
            numLB.textColor = .white
            numLB.backgroundColor = UIColor(hexString: "#B9B9B9")
            numLB.layer.cornerRadius = 7
            numLB.layer.masksToBounds = true
            numLB.snp.makeConstraints { (maker) in
                maker.left.equalTo(label.snp.right).offset(5)
                maker.centerY.equalTo(label)
                maker.right.equalToSuperview().offset(-10)
                maker.width.greaterThanOrEqualTo(14)
                maker.height.equalTo(14)
            }
        }
    }
    
    override func setStyle(isNormal: Bool) {
        self.backgroundColor = isNormal ? UIColor(hexString: "#F3F3F3") : UIColor(hexString: "#EEF0FC")
        label.textColor = isNormal ? UIColor.black : UIColor(hexString: "#27347D")
        numLB?.backgroundColor = isNormal ? UIColor(hexString: "#B9B9B9") : UIColor(hexString: "#27347D")
    }
}

class TTwoViewController: UIViewController {

    @IBOutlet weak var testLB: UILabel!
    var sv: SegementedView!
    var testV: TTTTestV!
    override func viewDidLoad() {
        super.viewDidLoad()
        let datasource = ["未拜访拜访拜访","进行中","已完成akjfhajsdflkajls;fkjakl;j", "lskfjls" ]
        
        var lbs:[TestV] = []
        for item in datasource {
            let lb = TestV(name: item)
            lbs.append(lb)
        }
        sv = SegementedView(items: lbs)
        self.view.addSubview(sv)
        sv.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(100)
            make.left.right.equalToSuperview()
            make.height.equalTo(30)
        }
        sv.clickBack = {[weak self] index in
            guard self != nil else { return }
            
            print("点击了 \(index)")
        }
        
        testLB.text = "dasfasfdasdfasdf"
        
        
        testV = TTTTestV()
        self.view.addSubview(testV)
        testV.snp.makeConstraints { (maker) in
            maker.left.right.equalToSuperview()
            maker.bottom.equalToSuperview().offset(-20)
            maker.height.equalTo(50)
        }
    }
    @IBAction func testAction(_ sender: Any) {
        
        if let lb = sv.itemWithIndex(index: 0) as? TestV {
            lb.setupCommentField(num: 10)
        }
    }
    
    @IBAction func backClicked(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        backToHomeList()
    }
    
}

class TTTTestV: UIView {
    var viewContainer: UIView!
    var label: UILabel!
    override func draw(_ rect: CGRect) {
        print("db-tttt", viewContainer.bounds)
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupViewContainer()
        setupCommentField()
        self.backgroundColor = UIColor.green
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViewContainer() {
        viewContainer = UIView()
        viewContainer.backgroundColor = UIColor.blue
        self.addSubview(viewContainer)
    }
    func setupCommentField() {
        label = UILabel()
        viewContainer.addSubview(label)
        label.backgroundColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.text = "hello world, today is a new day. Have a ni"
        self.addSubview(label)
        label.snp.makeConstraints { (make) in
            let superView = viewContainer!
            make.left.equalTo(superView).offset(10)
            make.right.equalTo(superView).offset(-10)
            make.centerY.equalTo(superView)
        }
        
        viewContainer.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(10)
            make.height.equalTo(40)
        }
    }

}
