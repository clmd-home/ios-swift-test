//
//  HandleCrash.h
//  swift-test
//
//  Created by clmd on 2020/6/2.
//  Copyright © 2020 clmd铭. All rights reserved.
//

#import <Foundation/Foundation.h>
 
@interface HandleCrash : NSObject
 
+ (NSString *)crash:(int)signal;
 
@end
