//
//  socket.swift
//  swift-test
//
//  Created by clmd on 2020/9/9.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import Foundation
import UIKit
import CocoaAsyncSocket

//let socketShared = Socket(hostUrl: "10.198.198.167", port: 11812)
let socketShared = Socket(hostUrl: "192.168.8.1", port: 11812) // hub

class Socket: NSObject {
    var socket: GCDAsyncSocket!
    
    let url: String
    let myport: UInt16
    
    private var heatBeat: Timer!
    
    var msgBack: (_ msgData: Data)->Void = {_ in}
    
    init(hostUrl: String, port: UInt16) {
        url = hostUrl
        myport = port
        super.init()
        if socket != nil {
            return
        }
        socket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.global())
        do {
            print("socket 开始连接")
            try socket.connect(toHost: hostUrl, onPort: port, withTimeout: 5)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // 写入消息
    func sendMessage(_ msg: String, tag: Int = 1){
        guard socket.isConnected else {
            return
        }
        
        let data = msg.data(using: .utf8)
        if tag == 1 {
            print("socket 待写消息: \(String(data: data ?? Data(), encoding: .utf8) ?? "")")
        }
        socket.write(data, withTimeout: -1, tag: tag)
        
    }
    
    // 初始化心跳包 5s一次
    private func initHeart(){
        heatBeat = Timer.init(timeInterval: 5, repeats: true, block: {[weak self] _ in
            guard let `self` = self else { return }
            self.sendMessage(["ping":1].toString(), tag: 0)
            print("socket 发送心跳")
        })
        RunLoop.current.add(heatBeat, forMode: .common)
    }
    // 销毁心跳
    func destoryHeart() {
        if heatBeat != nil {
            print("socket 断开心跳")
            heatBeat.invalidate()
            heatBeat = nil
        }
    }
    func reConnect(){
        do {
            print("socket 重新连接")
            try socket.connect(toHost: url, onPort: myport, withTimeout: 5)
        } catch {
            print(error.localizedDescription)
        }
    }
}

extension Socket: GCDAsyncSocketDelegate {

    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        print("socket 连接成功!")
        DispatchQueue.main.async {
            self.initHeart()
        }
        socket.readData(withTimeout: -1, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        print("socket 收到消息")
        print("message: " + (String(data: data, encoding: .utf8) ?? ""))
        self.msgBack(data)
        socket.readData(withTimeout: -1, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        if tag != 0 {
            print("socket 写入消息")
        }
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        print("socket 断开连接")
        destoryHeart()
        print(err?.localizedDescription ?? "")
        reConnect()
    }
}


