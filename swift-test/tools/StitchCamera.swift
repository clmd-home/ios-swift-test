//
//  StitchCamera.swift
//  Retail_REA
//
//  Created by clobotics_ccp on 2019/3/27.
//  Copyright © 2019 ice.hu. All rights reserved.
//

import AVFoundation
import Vision
import UIKit

protocol StitchCameraProxy: class {
    func startFailed()
    func didFetchDataOutputSampleBuffer(_ imageBuffer: CVImageBuffer, scaledImage: UIImage, artwork: UIImage?)
//    func photoTakeProcess(image:UIImage)
}

final class StitchCamera: NSObject {

    weak var delegate: StitchCameraProxy?
    private let captureSession = AVCaptureSession()
    private let camera:AVCaptureDevice = {
        var device:AVCaptureDevice!
        
//        if #available(iOS 13.0, *), let tripleCameraDevice = AVCaptureDevice.default(.builtInTripleCamera, for: .video, position: .back) {
//            device = tripleCameraDevice
//        } else
        if #available(iOS 11.1, *), let trueDepthDevice = AVCaptureDevice.default(.builtInTrueDepthCamera, for: .video, position: .back) {
            device = trueDepthDevice
        } else if #available(iOS 10.2, *), let dualCameraDevice = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .back) {
            device = dualCameraDevice
        } else {
            device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)!
        }
        
        return device
    }()
    
    private let outputQueue = DispatchQueue(label: "com.clobotics.videosamplequeue")
    private var inView: UIView?
    private var needCallback = true
    private let sdk = StitchSDKBridge()
    
    private var photoDataOutput: AVCapturePhotoOutput!
    
    private var fpsSpace = 0
    private let fpsLimt = 5 // 限制隔多少帧返回一次结果
    private var isMix: Bool = true // 拍摄模式 true: 混合模式 false: 截帧模式  混合模式：拍摄用相机模式拍，平时用截帧输出；截帧模式：拍摄也用截帧输出
    private var inTake: Bool = false // 截帧模式拍照标识符 true：拍照 false：正常
    
    func start(in view: UIView,spec: VideoSpec, isMix: Bool = true) {
//        // Log.log(isMix ? "混合模式" : "截帧模式", key: "camera.init", type: .info)
        self.isMix = isMix
        var success = false
        defer {
            if !success {
                delegate?.startFailed()
            }
        }
        guard setupInput(spec: spec) else { return }
        guard setupOutput() else { return }
        if isMix {
            guard setupPhotoOutput() else { return }
        }

        setupPreview(in: view)
        captureSession.startRunning()
        changeExposure(0)
        success = true
        
    }
    
    func start() {
        captureSession.startRunning()
    }
    
    func stop(){
        captureSession.stopRunning()
    }
    
    private func setupInput(spec: VideoSpec) -> Bool {
        do {
            let input = try AVCaptureDeviceInput(device: camera)
            guard self.captureSession.canAddInput(input)  else {
                return false
            }
            captureSession.addInput(input)
            if captureSession.canSetSessionPreset(.hd4K3840x2160){
                captureSession.sessionPreset = AVCaptureSession.Preset.hd4K3840x2160
            } else if captureSession.canSetSessionPreset(.hd1920x1080) {
                captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080
            }
            
            camera.updateFormatWithPreferredVideoSpec(preferredSpec: spec)
            return true
        }
        catch let error{
            // Log.log("\(error)", key: "setup device input error", type: .error)
        }
        return false
    }
    
    private func setupOutput() -> Bool {
        let videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_32BGRA)]
        videoDataOutput.alwaysDiscardsLateVideoFrames = true
        videoDataOutput.setSampleBufferDelegate(self, queue: outputQueue)
        videoDataOutput.connection(with: AVMediaType.metadata)?.preferredVideoStabilizationMode = .auto
        
        guard captureSession.canAddOutput(videoDataOutput) else {
            return false
        }
        captureSession.addOutput(videoDataOutput)
        videoDataOutput.connection(with: AVMediaType.video)?.videoOrientation = .portrait
        return true
    }
    
    private func setupPhotoOutput() -> Bool {
        photoDataOutput = AVCapturePhotoOutput()
        photoDataOutput.isHighResolutionCaptureEnabled = true
        
        if #available(iOS 13.0, *) {
            photoDataOutput.maxPhotoQualityPrioritization = .quality
        } else {
            // Fallback on earlier versions
        }
        guard captureSession.canAddOutput(photoDataOutput) else {
            // Log.log("", key: "AddOutput failed", type: .error)
            return false
        }
        photoDataOutput.connection(with: .metadata)?.preferredVideoStabilizationMode = .auto

        captureSession.addOutput(photoDataOutput)
        return true
    }
    
    private func setupPreview(in view: UIView) {
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.bounds
        previewLayer.contentsGravity = CALayerContentsGravity.resizeAspectFill
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        view.layer.insertSublayer(previewLayer, at: 0)
    }

    func changeExposure(_ value: Float) {
        do {
            try self.camera.lockForConfiguration()
            camera.setExposureTargetBias(value, completionHandler: nil)
            camera.unlockForConfiguration()
        } catch let error {
            // Log.log("\(error)", key: "resetExposure error", type: .error)
        }
    }
    
    func takePhoto() {
        guard isMix else {
            inTake = true
            return
        }
        
        //setup photo output
        let photoSettings = AVCapturePhotoSettings()
        photoSettings.isAutoStillImageStabilizationEnabled = true
        photoSettings.isHighResolutionPhotoEnabled = true
        if #available(iOS 13.0, *) {
            photoSettings.photoQualityPrioritization = .speed
        } else {
            // Fallback on earlier versions
        }
        
        guard let capturePhotoOutput = self.photoDataOutput else { return }
        capturePhotoOutput.capturePhoto(with: photoSettings, delegate: self)
    }
    
    func openTorch(_ commplete:(Bool) -> ()) {
        camera.openTorch(commplete)
    }
    
    var exposureTargetBias: Float {
        return camera.exposureTargetBias
    }
    
    var isTorchModeSupported: Bool {
        return camera.isTorchModeSupported(.on)
    }
    
    var maxExposureTargetBias: Float {
        return camera.maxExposureTargetBias
    }
    
    var minExposureTargetBias: Float {
        return camera.minExposureTargetBias
    }
    
    deinit {
        captureSession.stopRunning()
    }
}

extension StitchCamera: AVCaptureVideoDataOutputSampleBufferDelegate {

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
//        if #available(iOS 13.0, *) {
//            DEBUGLOG(message: sampleBuffer.formatDescription)
//        } else {
//            // Fallback on earlier versions
//        }
        
        if fpsSpace < fpsLimt {
            needCallback = false
            fpsSpace += 1
        } else {
            needCallback = true
            fpsSpace = 0
        }
        if needCallback {
            let  artworkImgBuf = CMSampleBufferGetImageBuffer(sampleBuffer)!
            
            // 原图
            let artwork = imageBufferToUIImage(artworkImgBuf, orientation: UIImage.Orientation.up).fixOrientation()
            let scaledsize = CGSize(width: 360, height: 640)
            if let scaledImg = artwork.scaledToSize(newSize: scaledsize), let imgBuf = CVPixelBufferRefFromUiImage(image: scaledImg, scaledsize) {
                DispatchQueue.main.async {
                    self.delegate?.didFetchDataOutputSampleBuffer(imgBuf, scaledImage: scaledImg, artwork: self.inTake ? artwork : nil)
                    self.inTake = false
                }
            } else {
                // Log.log("截帧模式,图片处理失败", key: "camera.didfetch", type: .error)
            }
        }
    }
    
}

extension StitchCamera: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
            
        // Make sure we get some photo sample buffer
        guard error == nil,
        let photoSampleBuffer = photoSampleBuffer else {
            print("Error capturing photo: \(String(describing: error))")
            return
        }
        // Convert photo same buffer to a jpeg image data by using // AVCapturePhotoOutput
        guard let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
                return
        }
        
        let scaledSize = CGSize(width: 360, height: 640)
        if let artwork = UIImage(data: imageData)?.fixOrientation(), let scaledImg = artwork.scaledToSize(newSize: scaledSize)?.fixOrientation(), let imgBuf = CVPixelBufferRefFromUiImage(image: scaledImg, scaledSize) {
            DispatchQueue.main.async {
                self.delegate?.didFetchDataOutputSampleBuffer(imgBuf, scaledImage: scaledImg, artwork: artwork)
            }
            
        } else {
            // Log.log("混合模式,图片处理失败", key: "camera.didfetch", type: .error)
        }
    }
}

extension StitchCamera {

    func CVPixelBufferRefFromUiImage(image: UIImage, _ size: CGSize) -> CVImageBuffer?{
        let img = image.cgImage!
        let options = [NSNumber.init(value: true): kCVPixelBufferCGImageCompatibilityKey, NSNumber.init(value: true): kCVPixelBufferCGBitmapContextCompatibilityKey] as NSDictionary
        var pxxbuffer: CVPixelBuffer!
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height), kCVPixelFormatType_32ARGB, options as CFDictionary, &pxxbuffer)
        assert(status == kCVReturnSuccess && pxxbuffer != nil)
        
        CVPixelBufferLockBaseAddress(pxxbuffer, CVPixelBufferLockFlags(rawValue: 0))
        let pxdata = CVPixelBufferGetBaseAddress(pxxbuffer)
        assert(pxdata != nil)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pxdata, width: Int(size.width), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: Int(4 * size.width), space: rgbColorSpace, bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue).rawValue)
        assert((context != nil))
        
        context?.draw(img, in: CGRect(x: 0, y: 0, width: img.width, height: img.height))
        
        CVPixelBufferUnlockBaseAddress(pxxbuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        return pxxbuffer
    }
    
    public func imageOrientationFromDeviceOrientation() -> UIImage.Orientation {
           let curDeviceOrientation = UIDevice.current.orientation
           let imageOrientation: UIImage.Orientation
           
           switch curDeviceOrientation {
           case UIDeviceOrientation.portraitUpsideDown:  // Device oriented vertically, home button on the top
               imageOrientation = .left
           case UIDeviceOrientation.landscapeLeft:       // Device oriented horizontally, home button on the right
               imageOrientation = .up
           case UIDeviceOrientation.landscapeRight:      // Device oriented horizontally, home button on the left
               imageOrientation = .down
           case UIDeviceOrientation.portrait:            // Device oriented vertically, home button on the bottom
               imageOrientation = .right
           default:
               imageOrientation = .up
           }
           return imageOrientation
       }

       func imageBufferToUIImage(_ imageBuffer: CVImageBuffer, orientation: UIImage.Orientation) -> UIImage {
         
           CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))

           let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
           let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)

           let width = CVPixelBufferGetWidth(imageBuffer)
           let height = CVPixelBufferGetHeight(imageBuffer)

           let colorSpace = CGColorSpaceCreateDeviceRGB()
           let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)
            
           let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
           let quartzImage = context!.makeImage()
           CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))

           let image = UIImage(cgImage: quartzImage!, scale: 1.0, orientation: orientation)
        
           return image
       }
}

extension UIImage {
    // 修复图片旋转
    func fixOrientation() -> UIImage {
        if self.imageOrientation == .up {
            return self
        }
         
        var transform = CGAffineTransform.identity
         
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: .pi)
            break
             
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
            break
             
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: -.pi / 2)
            break
             
        default:
            break
        }
         
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            break
             
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1)
            break
             
        default:
            break
        }
         
        let ctx = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: self.cgImage!.colorSpace!, bitmapInfo: self.cgImage!.bitmapInfo.rawValue)
        ctx?.concatenate(transform)
         
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx?.draw(self.cgImage!, in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(size.height), height: CGFloat(size.width)))
            break
             
        default:
            ctx?.draw(self.cgImage!, in: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(size.width), height: CGFloat(size.height)))
            break
        }
         
        let cgimg: CGImage = (ctx?.makeImage())!
        let img = UIImage(cgImage: cgimg)
         
        return img
    }
    
    func scaledToSize(newSize: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newimg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newimg
    }
    
   
}
