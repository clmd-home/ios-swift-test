//
//  StitchModelRecognition.swift
//  StitchCameraSDK
//
//  Created by clmd on 2020/3/27.
//  Copyright © 2020 ice.hu. All rights reserved.
//
/*
 usage:
 stitch_model_recognition.setModelFile(path: "/Fruit/cvexport.manifest")
 stitch_model_recognition.delegate = self
 stitch_model_recognition.recognition(sampleBuffer: imageBuffer)
 
 代理里判断
 */


import Foundation

import CVSInference
protocol StitchModelRecognitionProxy: class {
    func recognitionResult(type: RecognitionType, accuracy: Float)
}

public enum RecognitionType: String {
    case normal = "Normal"
    case blurry = "Blurry"
    case faraway = "Faraway"
    case smallAngle = "Small-Angle"
    case largeAngle = "Large-Angle"
}


final class StitchModelRecognition: NSObject {
    
    var skill: CVSClassifier!
    weak var delegate: StitchModelRecognitionProxy?
    // 上次识别的type，如果超过一定次数为同样的type，认为模型卡死，关闭识别
    private var lastRecognitionStatu: RecognitionType = .normal
    private var recognitionNum = 0
    var typeNoChange = false
    
    public func setModelFile(path: String) {
        // create skill configuration
        let config = CVSClassifierConfig()
        config.modelFile.string = Bundle.main.bundlePath.appending(path)
        do {
            try config.buildWithError()
        } catch {
            // Log.// Log("\(error)", key: "setModelFile-config", type: .error)
        }
        // create skill instance
//        skill = CVSClassifier(config: config);
        do {
            try skill = CVSClassifier.init(config: config, error: ())
        } catch {
            // Log.// Log("\(error)", key: "setModelFile-skill", type: .error)
        }
    }
    
    public func recognition(sampleBuffer: CVImageBuffer) {
        do {
//             guard let imageBuffer = sampleBuffer else {
//                throw NSError(domain: "Can't convert to CVImageBuffer.", code: -1, userInfo: nil)
//            }
            let image = imageBufferToUIImage(sampleBuffer, orientation: imageOrientationFromDeviceOrientation())
//            // Log.dPrint("\(UIImageJPEGRepresentation(image, 0.6)), \(UIImageJPEGRepresentation(image, 1))")
            patternRecognition(image: image)
        } catch {
            // Log.// Log("\(error)", key: "BufferToUIImage", type: .error)
        }
    }
}

extension StitchModelRecognition {
    
    func patternRecognition(image: UIImage) {
        do {
            // set runner inputs
            skill?.threshold.value = 0.0
            skill?.maxReturns.value = 5
            skill?.image.image = image
            // run and report results
            skill?.run()
            
            var bestConfidence: Float = 0.0
            var bestIdentifier = ""
            
            for index in 0 ..< skill.confidences.countOfValue() {
                guard let confidence = skill?.confidences.value(at: index), let identifier = skill?.identifiers.string(at: index) else {
                    throw NSError(domain: "Can't get confidence/identifier.", code: -1, userInfo: nil)
                }
//                print("result: \(identifier):\(confidence)")
                // 取准确度最大的type
                if confidence > bestConfidence {
                    bestConfidence = confidence
                    bestIdentifier = identifier
                }
            }
//            print("result best: \(bestIdentifier):\(bestConfidence) \n")
            
            let currentType = RecognitionType(rawValue: bestIdentifier) ?? RecognitionType.normal
            if currentType == lastRecognitionStatu, currentType == .largeAngle { // 长时间输出大角度
                recognitionNum += 1
            } else {
                lastRecognitionStatu = currentType
                recognitionNum = 0
                typeNoChange = false
            }
            if recognitionNum > 200, lastRecognitionStatu == .largeAngle {
                typeNoChange = true
            }
            DispatchQueue.main.async {
                self.delegate?.recognitionResult(type: currentType, accuracy: bestConfidence)
            }
            
        } catch {
            // Log.// Log("\(error)", key: "PatternRecognition", type: .error)
        }
    }
    
    public func imageOrientationFromDeviceOrientation() -> UIImage.Orientation {
        let curDeviceOrientation = UIDevice.current.orientation
        let imageOrientation: UIImage.Orientation
        
        switch curDeviceOrientation {
        case UIDeviceOrientation.portraitUpsideDown:  // Device oriented vertically, home button on the top
            imageOrientation = .left
        case UIDeviceOrientation.landscapeLeft:       // Device oriented horizontally, home button on the right
            imageOrientation = .up
        case UIDeviceOrientation.landscapeRight:      // Device oriented horizontally, home button on the left
            imageOrientation = .down
        case UIDeviceOrientation.portrait:            // Device oriented vertically, home button on the bottom
            imageOrientation = .right
        default:
            imageOrientation = .up
        }
        return imageOrientation
    }

    func imageBufferToUIImage(_ imageBuffer: CVImageBuffer, orientation: UIImage.Orientation) -> UIImage {
      
        CVPixelBufferLockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))

        let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer)
        let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer)

        let width = CVPixelBufferGetWidth(imageBuffer)
        let height = CVPixelBufferGetHeight(imageBuffer)

        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue)

        let context = CGContext(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)

        let quartzImage = context!.makeImage()
        CVPixelBufferUnlockBaseAddress(imageBuffer, CVPixelBufferLockFlags(rawValue: 0))

        let image = UIImage(cgImage: quartzImage!, scale: 1.0, orientation: orientation)

        return image
    }

    
}
