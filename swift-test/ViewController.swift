//
//  ViewController.swift
//  swift-test
//
//  Created by clmd on 2020/5/13.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit
import SnapKit
//import Alamofire
import NetworkExtension
import swiftScan
//import Hue
import libavif
import librav1e

class ViewController: UIViewController {

    @IBOutlet weak var answerTF: UITextField!
    @IBOutlet weak var cameraBTN: UIButton!
    @IBOutlet weak var wifiBtn: UIButton!
    @IBOutlet weak var jpegImageV: UIImageView!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var qSlider: UISlider!
    @IBOutlet weak var qLB: UILabel!
    
    var dataSource: [String] = ["slkfj", "dafdsfddd", "sss22222222", "dafdsfddd", "41241", "2222222222222"]
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewLeftAlignedLayout()
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 15, right: 10)
        layout.minimumInteritemSpacing = 60
        layout.minimumLineSpacing = 20
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1).withAlphaComponent(0.6)
        cv.isScrollEnabled = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.dataSource = self
        cv.delegate = self
        cv.register(UINib(nibName: "OptionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OptionCellID")
        return cv
    }()
    
    private let camera = StitchCamera()
    
    private let modelHandler = TFModelHandler(file: "efficientdet-d0-pgus")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let dd = [13213,23,23]
//        let aaa = dd[3]
//        print(aaa)
        
//    AF.download("https://fileman8.blob.core.chinacloudapi.cn/files/b598f9aeea8450a48d0c4601d13ab7a0.jpeg?sv=2017-04-17&sr=b&sig=YLDsKV6DT%2BHMeMmhVyI7uDJyygSaZ14WQPsfNdT8fRI%3D&st=2020-05-25T23%3A57%3A08Z&se=2020-05-26T12%3A02%3A08Z&sp=rw").responseData { response in
//            print("backed")
//            if let data = response.value {
//                let image = UIImage(data: data)
//                print(image?.size)
//            }
//        }
        
        
//        let ddd = UILabel()
//        ddd.text = "sadf a"
//        self.view.addSubview(ddd)
//        ddd.snp.makeConstraints { (make) in
//            make.center.equalToSuperview()
//        }
        
//        let aa = PolygonView(frame: CGRect.zero)
//        self.view.addSubview(aa)
//
//        aa.changeTo(potnts: [-4.6, 64.39999999999999, 368.0, 70.0, 365.7, 876.4, -4.6, 879.1999999999999], color: UIColor.green.withAlphaComponent(0.4))
        
//        let dd = Polygon(points: [-4.6, 64.39999999999999, 368.0, 70.0, 365.7, 876.4, -4.6, 879.1999999999999]).fill(with: Color.green)
//        let ppp = [dd].group()
//        let maV = MacawView()
//        maV.backgroundColor = UIColor.clear
//        maV.node = ppp
//        self.view.addSubview(maV)
//        maV.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview()
//        }
//        let pp = Polygon(points: [-4.6, 64.39999999999999, 66, 50, 365.7, 876.4, -4.6, 879.1999999999999]).fill(with: Color.red.with(a: 0.5))
//
////        ppp.contentsVar.animation(to: pp).autoreversed().cycle().play()
//        dd.formVar.animate(to: pp.form)
//        dd.fillVar.animate(from: dd.fill, to: pp.fill!, during: 0, delay: 0)
        
//        let btn = UIButton()
//        btn.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
//        btn.setTitle("crash", for: .normal)
//        btn.addTarget(self, action: #selector(signalBtnAct), for: .touchUpInside)
//        self.view.addSubview(btn)
//        btn.snp.makeConstraints { (make) in
//            make.center.equalToSuperview()
//            make.width.height.equalTo(60)
//        }
//
//        let btn1 = UIButton()
//        btn1.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
//        btn1.setTitle("crash", for: .normal)
//        btn1.addTarget(self, action: #selector(exceptionBtnAct), for: .touchUpInside)
//        self.view.addSubview(btn1)
//        btn1.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.top.equalTo(btn.snp.bottom).offset(30)
//            make.width.height.equalTo(60)
//        }
        
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(wifiBtn.snp.bottom).offset(5)
            make.bottom.left.right.equalToSuperview()
        }

        setupCamera()
        
        imageV.addGesture(.tap) { (tap) in
            let view = UIView(frame: CGRect(origin: self.view.bounds.origin, size: self.view.bounds.size))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            ImageZoom.show(image: self.imageV.image, in: view)
            UIApplication.shared.keyWindow?.addSubview(view)
            view.addGesture(.tap) { (tap) in
                view.removeFromSuperview()
            }
        }
        jpegImageV.addGesture(.tap) { (tap) in
            let view = UIView(frame: CGRect(origin: self.view.bounds.origin, size: self.view.bounds.size))
            view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            ImageZoom.show(image: self.jpegImageV.image, in: view)
            UIApplication.shared.keyWindow?.addSubview(view)
            view.addGesture(.tap) { (tap) in
                view.removeFromSuperview()
            }
        }
        
    }
    
    @IBAction func sliderChange(_ sender: UISlider) {
        qLB.text = String(format: "%.1f", sender.value * 100)
    }
    func setupCamera() {
//        let spec = VideoSpec(fps: 20, size: CGSize(width: 1920, height: 1080))
        let spec = VideoSpec(fps: 20, size: CGSize(width: 1280, height: 720))
        camera.delegate = self
        camera.start(in: self.view, spec: spec, isMix: false)
    }
    @IBAction func takePhotoClicked(_ sender: Any) {
//        let vc = ARCameraViewController()
//        vc.callBack = {[weak self] answer, img in
//            guard let `self` = self else { return }
//            self.answerTF.text = answer
//            self.cameraBTN.setBackgroundImage(img, for: .normal)
//
//        }
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        
//        let vc = TaskCareListViewController()
        let vc = TTwoViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func exceptionBtnAct(){
//        let ttaar = NSArray()
//        print(ttaar[66])
        let vc = ARCameraViewController()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    @objc func signalBtnAct(){
        
        let filePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        //crash存储位置
        print(filePath)
        
        var strr:String!
        strr = strr + "...."
        
    }
    @IBAction func socketConnectClicked(_ sender: UIButton) {
        socketShared.msgBack = {[weak self] msg in
            guard let `self` = self else { return }
            print("vc: \(String(data: msg, encoding: .utf8) ?? "")")
            let dic = try? JSONSerialization.jsonObject(with: msg, options: .mutableContainers) as? [String: Any]
            print("\(dic)")
            
        }
        
        if !socketShared.socket.isConnected {
            socketShared.reConnect()
        }
//        print(socketShared.socket.readyState.rawValue)
//        if socketShared.socket.readyState.rawValue == 3 {
//            socketShared.reConnect()
//        }
//        let sss = Socket(myURL: "192.168.8.1")
    }
    @IBAction func closeClicked(_ sender: UIButton) {
//        socketShared.socket.disconnect()
        
//        let uuid = UUID()
//        let msg = ["requestId": "\(uuid)", "action": "get_registry_info"].toString()
//        socketShared.sendMessage(msg)
        
//        self.present(scanViewController(), animated: true, completion: nil)
        
//        let vc = TaskCareListViewController()
        
//        let vc = TTwoViewController()
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        
//        if let image = UIImage(named: "加载0003.png"), let buffer = CVPixelBufferRefFromUiImage(image: image, image.size) {
//            let dd = StitchSDKBridge()
////            dd.handleTest(buffer)
//            let rgba = StitchSDKBridge.getRGBAWith(image)
//            let rgbarr = StitchSDKBridge.rgbArray(image)
//            print(rgba)
//        }
        
        camera.takePhoto()
    }
    
    @IBAction func connectWifiClicked(_ sender: UIButton) {
        getWifiList()
        
//        let hospotConfig = NEHotspotConfiguration(ssid: "CLB_S30_07C3")
//        NEHotspotConfigurationManager.shared.apply(hospotConfig) { (err) in
//            print(err?.localizedDescription ?? "")
//        }
    }
    
    func getWifiList() {
        let ss: NSString = "clmd-clb"
        NEHotspotHelper.register(options: [kNEHotspotHelperOptionDisplayName: ss], queue: DispatchQueue.global()) { (cmd) in
            for net in cmd.networkList! {
                print(net)
            }
        }
    }
    
    func toAVIF(){
        
    }
    
    
    func CVPixelBufferRefFromUiImage(image: UIImage, _ size: CGSize) -> CVImageBuffer?{
        let img = image.cgImage!
        let options = [NSNumber.init(value: true): kCVPixelBufferCGImageCompatibilityKey, NSNumber.init(value: true): kCVPixelBufferCGBitmapContextCompatibilityKey] as NSDictionary
        var pxxbuffer: CVPixelBuffer!
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(size.width), Int(size.height), kCVPixelFormatType_32ARGB, options as CFDictionary, &pxxbuffer)
        assert(status == kCVReturnSuccess && pxxbuffer != nil)
        
        CVPixelBufferLockBaseAddress(pxxbuffer, CVPixelBufferLockFlags(rawValue: 0))
        let pxdata = CVPixelBufferGetBaseAddress(pxxbuffer)
        assert(pxdata != nil)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pxdata, width: Int(size.width), height: Int(size.height), bitsPerComponent: 8, bytesPerRow: Int(4 * size.width), space: rgbColorSpace, bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue | CGBitmapInfo.byteOrder32Little.rawValue).rawValue)
        assert((context != nil))
        
        context?.draw(img, in: CGRect(x: 0, y: 0, width: img.width, height: img.height))
        
        CVPixelBufferUnlockBaseAddress(pxxbuffer, CVPixelBufferLockFlags(rawValue: 0))
        
        return pxxbuffer
    }
    @IBAction func backAction(_ sender: Any) {
        backToHomeList()
    }
    
}


extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OptionCellID", for: indexPath) as! OptionCollectionViewCell
        cell.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        cell.titleLB.text = dataSource[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellH = 30
        let titleW = dataSource[indexPath.row].count * 14 + 20
        return CGSize(width: titleW, height: cellH)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension ViewController: StitchCameraProxy {
    func startFailed() {
        
    }
    
    func didFetchDataOutputSampleBuffer(_ imageBuffer: CVImageBuffer, scaledImage: UIImage, artwork: UIImage?) {
        if artwork != nil {
            
            if let buffer = CVImageBuffer.buffer(from: artwork ?? UIImage()), let result = modelHandler?.runModel(onFrame: buffer, imageConfig: ImageConfig()) {
                print(result)
            }
            
            let img = artwork ?? UIImage()
            print("take photo")
            print("noml size:", (img.jpegData(compressionQuality: 1)?.count ?? 0)/1024, " kb")
//            if let jpegdata = img.jpegData(compressionQuality: CGFloat(self.qSlider.value)) {
            if let jpegdata = img.jpegData(compressionQuality: img.size.height > 1920 ? 0.6 : 0.7) {
                print("jpeg size:", jpegdata.count / 1024, " kb", "quality:", img.size.height > 1920 ? 0.6 : 0.7)
                jpegImageV.image = UIImage(data: jpegdata)
            }
            
            let dd = StitchSDKBridge.getRGBAWith(img)
            let sdk = StitchSDKBridge()
//            sdk.handleTest(dd, size: img.size, fileName: "test")
            if let data = sdk.webpEncode(dd, size: img.size, quality: self.qSlider.value * 100) {
                print("webP size:", data.count / 1024, " kb", "quality:", self.qSlider.value * 100)
                let image = sdk.webpDecode(data)
                imageV.image = image
            }
        }
    }
}

class scanViewController: LBXScanViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "识别二维码/条码"
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.tintColor = .white
        self.isOpenInterestRect = true
        
    }
    
    override func handleCodeResult(arrayResult: [LBXScanResult]) {
        if let result = arrayResult.first, let msg = result.strScanned{
            navigationController?.popViewController(animated: true)
        }
    }
}

