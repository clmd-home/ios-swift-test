//
//  ModelRecognitionViewController.swift
//  swift-test
//
//  Created by clmd on 2021/2/1.
//  Copyright © 2021 clmd铭. All rights reserved.
//

import UIKit

class ModelRecognitionViewController: UIViewController {

    @IBOutlet weak var iouThreshSlider: UISlider!
    @IBOutlet weak var iouThreshValueLB: UILabel!
    
    private let camera = StitchCamera()
    private let modelHandler = TFModelHandler(file: "efficientdet-d0-pgus", threadCount: 5)
    private let skuModelHandler = TFModelHandler(file: "mobilenet_v2_fbc_latest", threadCount: 5)
    
    private var isTake = false
    
    private var cameraView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
        setupMNNModel()
        
        iouThreshValueLB.text = "\(iouThreshSlider.value)"
        
        cameraView.frame = CGRect(x: 0, y: 200, width: mainWidth, height: mainWidth)
        cameraView.backgroundColor = .black
        self.view.addSubview(cameraView)
        
    }
    
    
    // nms 非极大值抑制
    func hanlde(data: [Float], iou_thresh: Float = 0.5) ->[[Float]] {
            
        var formatData:[[Float]] = []

        // 将源数据提取出关键数据 组成 items [ymin, xmin, ymax, xmax, confidence score]
        var tempData: [Float] = []
        for (index,item) in data.enumerated() {
            if index % 7 == 0 {
                tempData = []
                continue
            }
            if index % 7 == 6 {
                formatData.append(tempData)
                continue
            }
            tempData.append(item)
        }
        formatData = formatData.sorted(by: {$0.last! > $1.last!})

        var nmsData: [[Float]] = []
        
        todata:
        for item in formatData{

            for nmsItem in nmsData{
                let overlapY1 = max(item[0], nmsItem[0])
                let overlapX1 = max(item[1], nmsItem[1])
                let overlapY2 = min(item[2], nmsItem[2])
                let overlapX2 = min(item[3], nmsItem[3])

                let w = max((overlapX2 - overlapX1) + 1, 0)
                let h = max((overlapY2 - overlapY1) + 1, 0)
                let intersection = w * h// 重叠面积

                let area1_W = max(item[3] - item[1] + 1, 0)
                let area1_H = max(item[2] - item[0] + 1, 0)
                let area1 = area1_H * area1_W

                let area2_W = max(nmsItem[3] - nmsItem[1] + 1, 0)
                let area2_H = max(nmsItem[2] - nmsItem[0] + 1, 0)
                let area2 = area2_H * area2_W

                if intersection / (area1 + area2 - intersection) > iou_thresh {
                    continue todata
                }
            }
            
            nmsData.append(item)
        }

        return nmsData
    }
    
    func setupCamera() {
//        let spec = VideoSpec(fps: 20, size: CGSize(width: 1920, height: 1080))
        let spec = VideoSpec(fps: 20, size: CGSize(width: 1280, height: 720))
        camera.delegate = self
        let cameraV = UIView(frame: CGRect(x: mainWidth / 16 * 7 / 2, y: 0, width: mainWidth / 16 * 9, height: mainWidth))
        cameraView.addSubview(cameraV)
        camera.start(in: cameraV, spec: spec, isMix: false)
    }
    
    func setupMNNModel(){
        ImageQualityEvaluatorApi.initModel(Bundle.main.path(forResource: "yolo", ofType: "mnn")!, yoloConfigPath: Bundle.main.path(forResource: "yolo", ofType: "json")!, unetModelPath: Bundle.main.path(forResource: "unet", ofType: "mnn")!)
    }
    
    @IBAction func iouThreshChange(_ sender: UISlider) {
        iouThreshValueLB.text = String("\(sender.value)".prefix(5))
    }
    
    @IBAction func takePhotoClicked(_ sender: Any) {
        camera.takePhoto()
        isTake = true
        
    }
    
    @IBAction func takebackClicked(_ sender: Any) {
        if let vvvv = self.view.viewWithTag(1000000) {
            vvvv.removeFromSuperview()
            isTake = false
            camera.start()
        }
    }
    
    func rrrr(image: UIImage, superView: UIView, skuRecognition:Bool = false) {
        if let buffer = CVImageBuffer.buffer(from: image), let result = modelHandler?.runModel(onFrame: buffer, imageConfig: ImageConfig()) {
            let viewpoints = hanlde(data: result, iou_thresh: iouThreshSlider.value)
//            print(viewpoints.count)
            for item in superView.subviews {
                if item.tag > 0 {
                    item.removeFromSuperview()
                }
            }
            
            let scale: CGFloat = mainWidth / 512
            for (index, item) in viewpoints.enumerated() {
                let rect: CGRect = CGRect(x: CGFloat(item[1]) * scale, y: CGFloat(item[0]) * scale, width: CGFloat(item[3] - item[1]) * scale, height: CGFloat(item[2] - item[0]) * scale)
                let pointView = UIView(frame: rect)
                pointView.backgroundColor = UIColor.green.withAlphaComponent(0.2)
                pointView.layer.borderWidth = 2
                pointView.layer.borderColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
                pointView.tag = index + 1
                superView.addSubview(pointView)
                
                if !skuRecognition { continue }
                // 截图
                let skuScaleH: CGFloat = image.size.height / 512
                let skuScaleW: CGFloat = image.size.width / 512
                
                let cropRect = CGRect(x: CGFloat(item[1]) * skuScaleW, y: CGFloat(item[0]) * skuScaleH, width: CGFloat(item[3] - item[1]) * skuScaleW, height: CGFloat(item[2] - item[0]) * skuScaleH)
                if let cropCGImage = image.cgImage?.cropping(to: cropRect) {
                    let cropImage =  UIImage(cgImage: cropCGImage)
                    
                    if let cropBuffer = CVImageBuffer.buffer(from: cropImage), let skuResult = skuModelHandler?.runModel(onFrame: cropBuffer, imageConfig: ImageConfig(batchSize: 1, inputChannels: 3, inputWidth: 224, inputHeight: 224)) {
                        
                        let zipResult = zip(skuResult.indices, skuResult)
                        let storResut = zipResult.sorted(by: {$0.1 > $1.1})
                        
                        let lab = UILabel()
                        lab.text = "\(storResut.first?.0 ?? 0)"
                        lab.textColor = .red
                        pointView.addSubview(lab)
                        lab.snp.makeConstraints({$0.center.equalToSuperview()})
                    }
                }
            }
            
        }
    }
}

extension ModelRecognitionViewController: StitchCameraProxy {
    func startFailed() {
        
    }
    
    func didFetchDataOutputSampleBuffer(_ imageBuffer: CVImageBuffer, scaledImage: UIImage, artwork: UIImage?) {
//        let img = camera.imageBufferToUIImage(imageBuffer, orientation: .left)
        
        let img = scaledImage
        let size = max(img.size.height, img.size.width)
        
        if let img = artwork {
            let ddd = ImageQualityEvaluatorApi.evaluate(toInt: img, angleThresh: 20, fgdRatioThresh: 0.3)
            print(ddd)
            
        }
        
        return
        
        let paddedImg = img.drawImageOnCanvas(canvasSize: CGSize(width: size, height: size), canvasColor: .black)
        if !isTake {
            rrrr(image: paddedImg, superView: cameraView)
        }
        
        if let img = artwork{
            camera.stop()
            let vvvv = UIImageView()
            vvvv.frame = CGRect(x: 0, y: 200, width: mainWidth, height: mainWidth)
            vvvv.image = paddedImg
            vvvv.tag = 1000000
            self.view.addSubview(vvvv)
            vvvv.addGesture(.tap) { (gr) in
                vvvv.removeFromSuperview()
                self.isTake = false
                self.camera.start()
            }
            print("start \(Date())")
            rrrr(image: paddedImg, superView: vvvv, skuRecognition: true)
            print("end \(Date())")
            
        }
    }
}
