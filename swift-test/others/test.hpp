//
//  test.hpp
//  PHPickerDemo
//
//  Created by Valeria on 2021/2/25.
//  Copyright © 2021 Apple. All rights reserved.
//

#ifndef test_hpp
#define test_hpp

#include <vector>

void demo(std::vector<float> &result);

void test();
#endif /* test_hpp */
