//
//  ImageQualityEvalutor.hpp
//  PHPickerDemo
//
//  Created by LIN Wenfeng on 2021/3/1.
//  Copyright © 2021 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageQualityEvaluatorApi : NSObject
{
}

+(NSString *) initModel: (NSString *) yoloModelPath yoloConfigPath: (NSString *) yoloConfigPath unetModelPath: (NSString *) unetModelPath;
+(UIImage *) evaluate: (UIImage *) image;
+(int) evaluateToInt:(UIImage *)image angleThresh: (int)angleThresh fgdRatioThresh: (float)fgdRatioThresh;

@end

NS_ASSUME_NONNULL_END
