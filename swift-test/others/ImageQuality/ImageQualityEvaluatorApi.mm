//
//  ImageQualityEvaluator.m
//  PHPickerDemo
//
//  Created by LIN Wenfeng on 2021/3/1.
//  Copyright © 2021 Apple. All rights reserved.
//

#import <iostream>
#import "ImageQualityEvaluatorApi.h"
#import "ImageQualityEvaluator/yolov5.hpp"
#import "ImageQualityEvaluator/unet.hpp"
#import "ImageQualityEvaluator/evaluator.hpp"
#import <opencv2/imgproc.hpp>

static YOLOv5 *yolo = new YOLOv5();
static Unet *unet = new Unet();

@implementation ImageQualityEvaluatorApi
+(int) evaluateToInt:(UIImage *)image angleThresh: (int)angleThresh fgdRatioThresh: (float)fgdRatioThresh  {
    std::vector<float> result(1);
    result[0] = float(Evaluator::ImageQuality::IMAGE_QUALITY_OK);

    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    cv::Mat cvMat(rows, cols, CV_8UC4);
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                 cols,                       // Width of bitmap
                                                 rows,                       // Height of bitmap
                                                 8,                          // Bits per component
                                                 cvMat.step[0],              // Bytes per row
                                                 colorSpace,                 // Colorspace
                                                 kCGImageAlphaNoneSkipLast |
                                                 kCGBitmapByteOrderDefault); // Bitmap info flags
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);

    std::string error = yolo->infer((uint8_t *) cvMat.data, cvMat.cols, cvMat.rows, result);
    if (!error.empty()) {
        printf("ImageQuality: Failed to run YOLO: %s", error.c_str());
        result[0] = -1;
    }
    else {
        error = unet->infer((uint8_t *) cvMat.data, cvMat.cols, cvMat.rows, result);
        if (!error.empty()) {
            printf("ImageQuality: Failed to run U-Net: %s", error.c_str());
            result[0] = -1;
        }
        else
            Evaluator::evaluate(result, angleThresh, fgdRatioThresh);
//            Evaluator::evaluate(result, 20, 0.3);
    }
    return result[0];
}

+(UIImage *) evaluate: (UIImage *) image
{
    std::vector<float> result(1);
    result[0] = float(Evaluator::ImageQuality::IMAGE_QUALITY_OK);

    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    cv::Mat cvMat(rows, cols, CV_8UC4);
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                 cols,                       // Width of bitmap
                                                 rows,                       // Height of bitmap
                                                 8,                          // Bits per component
                                                 cvMat.step[0],              // Bytes per row
                                                 colorSpace,                 // Colorspace
                                                 kCGImageAlphaNoneSkipLast |
                                                 kCGBitmapByteOrderDefault); // Bitmap info flags
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);

    std::string error = yolo->infer((uint8_t *) cvMat.data, cvMat.cols, cvMat.rows, result);
    if (!error.empty()) {
        printf("ImageQuality: Failed to run YOLO: %s", error.c_str());
        result[0] = -1;
    }
    else {
        error = unet->infer((uint8_t *) cvMat.data, cvMat.cols, cvMat.rows, result);
        if (!error.empty()) {
            printf("ImageQuality: Failed to run U-Net: %s", error.c_str());
            result[0] = -1;
        }
        else
//            Evaluator::evaluate(result, angleThresh, fgdRatioThresh);
            Evaluator::evaluate(result, 20, 0.3);
    }
    
    int pos = 1;
    int numSku = int(result[pos]);
    pos++;
    for (int i = 0; i < numSku; i++) {
        float l = result[pos + i * 5 + 1];
        float t = result[pos + i * 5 + 2];
        float r = result[pos + i * 5 + 3];
        float b = result[pos + i * 5 + 4];
        cv::rectangle(cvMat, cv::Point(cols * l, rows * t), cv::Point(cols * r, rows * b), cv::Scalar(0, 255, 0), 3);
    }
    pos += 5 * numSku;
    int numLine = int(result[pos]);
    pos += 1;
    for (int i = 0; i < numLine; i++) {
        float l = result[pos + i * 5 + 1];
        float t = result[pos + i * 5 + 2];
        float r = result[pos + i * 5 + 3];
        float b = result[pos + i * 5 + 4];
        cv::line(cvMat, cv::Point(cols * l, rows * t), cv::Point(cols * r, rows * b), cv::Scalar(255, 0, 0), 5);
    }

    NSData *data = [NSData dataWithBytes: cvMat.data length: cvMat.elemSize() * cvMat.total()];
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                       cvMat.rows,                                 //height
                                       8,                                          //bits per component
                                       8 * cvMat.elemSize(),                       //bits per pixel
                                       cvMat.step[0],                            //bytesPerRow
                                       colorSpace,                                 //colorspace
                                       kCGImageAlphaNoneSkipLast|kCGBitmapByteOrderDefault,// bitmap info
                                       provider,                                   //CGDataProviderRef
                                       NULL,                                       //decode
                                       false,                                      //should interpolate
                                       kCGRenderingIntentDefault                   //intent
                                       );
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    return finalImage;
}

+(NSString *) initModel: (NSString *) yoloModelPath yoloConfigPath: (NSString *) yoloConfigPath unetModelPath: (NSString *) unetModelPath
{
    std::string error = yolo->init(std::string([yoloModelPath UTF8String]), std::string([yoloConfigPath UTF8String]));
    if (error.empty())
        error = unet->init(std::string([unetModelPath UTF8String]), "");
    return [NSString stringWithCString:error.c_str() encoding:[NSString defaultCStringEncoding]];
}

@end
