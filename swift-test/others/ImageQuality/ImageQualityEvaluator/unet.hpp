#ifndef __UNET_H__
#define __UNET_H__

#include "basemodel.hpp"

extern "C" {

class Unet : public BaseModel
{
protected:
    virtual std::string loadModel(const std::string &modelPath);
    virtual std::string loadConfig(const std::string &configPath);
    virtual std::string preprocess(const uint8_t *image, int width, int height);
    virtual std::string postprocess(int width, int height, std::vector<float> &result);
    virtual std::string simulate(std::vector<float> &result);

};
}

#endif
