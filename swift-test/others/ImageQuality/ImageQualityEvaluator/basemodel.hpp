#ifndef __BASEMODEL_H__
#define __BASEMODEL_H__

#include <fstream>
#include <string>
#include <vector>
#include <MNN/ImageProcess.hpp>
#include <MNN/Interpreter.hpp>
#include <MNN/Tensor.hpp>
#include "json11.hpp"
#include <opencv2/core.hpp>

extern "C" {

class BaseModel
{
protected:
    bool mEnabled;
    bool mInitialized;
    MNN::Interpreter *mInterpreter;
    MNN::Session * mSession;
    MNN::CV::ImageProcess *mPreprocessor;
    std::map<std::string, MNN::Tensor *> mInputTensors;
    std::map<std::string, MNN::Tensor *> mOutputTensors;

    int mWidth;
    int mHeight;

    virtual void initPreprocessor();
    virtual std::string loadModel(const std::string &modelPath);
    virtual std::string loadConfig(const std::string &configPath);
    virtual std::string preprocess(const uint8_t *image, int width, int height);
    virtual std::string postprocess(int width, int height, std::vector<float> &result);
    virtual std::string simulate(std::vector<float> &result);

public:
    BaseModel();
    ~BaseModel();

    virtual std::string init(const std::string &modelPath, const std::string &configPath);
    virtual std::string infer(uint8_t *image, int width, int height, std::vector<float> &result);
};
}

#endif
