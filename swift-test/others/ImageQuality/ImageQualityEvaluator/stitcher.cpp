#include "stitcher.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"

extern "C" {

Stitcher::Stitcher() : mInputWidth(1080), mInputHeight(1920), mActualWidth(270), mActualHeight(480)
{
    mInputFrame = cv::Mat(mInputHeight, mInputWidth, CV_8UC3);
    mActualFrame = cv::Mat(mActualHeight, mActualWidth, CV_8UC3);
    mFeatureDetector = cv::ORB::create(1000);
    mMatcher = cv::BFMatcher::create(cv::NORM_HAMMING, false);
}

Stitcher::~Stitcher()
{}

void Stitcher::preprocess(const uint8_t *image)
{
    cv::Mat tmp(mInputHeight, mInputWidth, CV_8UC4, (void *)image);
    cv::cvtColor(tmp, mInputFrame, cv::COLOR_RGBA2BGR);
    cv::resize(mInputFrame, mActualFrame, cv::Size(mActualWidth, mActualHeight));
}

std::string Stitcher::start(const uint8_t *image)
{
    mFrameQueue.clear();
    mKpQueue.clear();
    mFeatDescQueue.clear();

    if (!image)
        return std::string("Stitcher::start(): invalid image");
    preprocess(image);

    std::vector<cv::KeyPoint> keyPts;
    cv::Mat desc;
    mFeatureDetector->detectAndCompute(mActualFrame, cv::Mat(), keyPts, desc);
    mFrameQueue.push_back(mActualFrame.clone());
    mKpQueue.push_back(keyPts);
    mFeatDescQueue.push_back(desc);
    return std::string();
}

std::string Stitcher::stitch(const uint8_t *image, std::vector<float> &result)
{
    result.clear();

    if (!image)
        return std::string("Stitcher::stitch(): invalid input");
    if (mFeatDescQueue.empty()) return std::string();
    preprocess(image);

    std::vector<cv::KeyPoint> keyPts;
    cv::Mat desc;
    mFeatureDetector->detectAndCompute(mActualFrame, cv::Mat(), keyPts, desc);

    std::vector<std::vector<cv::DMatch>> matches;
    if (!desc.empty() && !mFeatDescQueue.back().empty()) {
        mMatcher->knnMatch(desc, mFeatDescQueue.back(), matches, 2);
        std::vector<cv::Point2f> srcPts, dstPts;
        cv::Mat inliers;
        for (const std::vector<cv::DMatch> &m : matches) {
            if (m[0].distance < 0.7 * m[1].distance) {
                srcPts.push_back(keyPts[m[0].queryIdx].pt);
                dstPts.push_back(mKpQueue.back()[m[0].trainIdx].pt);
            }
        }
        if (srcPts.size() > 10) {
            cv::Mat T = cv::estimateAffinePartial2D(srcPts, dstPts, inliers);
            if (cv::countNonZero(inliers) > 10) {
                float sin = T.at<double>(0, 0);
                float cos = T.at<double>(1, 0);
                float scale = std::sqrt(sin * sin + cos * cos);
                float rotation = 180.0f * std::atan2(cos, sin) / CV_PI;
                result.push_back(scale);
                result.push_back(rotation);
                result.push_back(T.at<double>(0, 2) / mActualWidth);
                result.push_back(T.at<double>(1, 2) / mActualHeight);
                for (int i = 0; i < srcPts.size(); i++) {
                    if (inliers.at<uint8_t>(i) != 0) {
                        result.push_back(srcPts[i].x / mActualWidth);
                        result.push_back(srcPts[i].y / mActualHeight);
                        result.push_back(dstPts[i].x / mActualWidth);
                        result.push_back(dstPts[i].y / mActualHeight);
                    }
                }
            }
        }
    }
    return std::string();
}

}
