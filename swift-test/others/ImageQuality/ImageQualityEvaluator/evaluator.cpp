#include "evaluator.hpp"
#include <cmath>

extern "C" {

void Evaluator::evaluate(std::vector<float> &info, float angleThresh, float fgdRatioThresh)
{
    int pos = 1;
    int numSku = int(info[pos]);
    pos++;
    float minX = 1.0f;
    float maxX = 0.0f;
    float minY = 1.0f;
    float maxY = 0.0f;
    for (int i = 0; i < numSku; i++) {
        float l = info[pos + i * 5 + 1];
        float t = info[pos + i * 5 + 2];
        float r = info[pos + i * 5 + 3];
        float b = info[pos + i * 5 + 4];
        minX = std::min(minX, l);
        maxX = std::max(maxX, r);
        minY = std::min(minY, t);
        maxY = std::max(maxY, b);
    }
    float fgdRatio = 0.0f;
    if (minX < maxX && minY < maxY) {
        fgdRatio = (maxX - minX) * (maxY - minY);
    }
    pos += 5 * numSku;
    int numLine = int(info[pos]);
    pos += 1;
    float maxAngle = -180.0f;
    float minAngle = 180.0f;
    for (int i = 0; i < numLine; i++) {
        float angle = info[pos + i * 5];
        maxAngle = std::max(maxAngle, angle);
        minAngle = std::min(minAngle, angle);
    }
    if (maxAngle < minAngle) {
        maxAngle = 0.0f;
        minAngle = 0.0f;
    }
    float deltaAngle = maxAngle - minAngle;
    float absAngle = std::max(std::abs(minAngle), std::abs(maxAngle));
    absAngle = std::max(absAngle, deltaAngle);
    if (absAngle > angleThresh) info[0] = float(ImageQuality::IMAGE_QUALITY_LARGE_ANGLE);
    else if (fgdRatio < fgdRatioThresh) info[0] = float(ImageQuality::IMAGE_QUALITY_LOW_RATIO);
    else info[0] = float(ImageQuality::IMAGE_QUALITY_OK);
}

}