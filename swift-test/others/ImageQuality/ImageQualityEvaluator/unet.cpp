#include "unet.hpp"
#include "opencv2/imgproc.hpp"

extern "C" {

std::string Unet::loadConfig(const std::string &configPath)
{
    return std::string();
}

std::string Unet::loadModel(const std::string &modelPath)
{
    // Load model
    mInterpreter = MNN::Interpreter::createFromFile(modelPath.c_str());
    if (!mInterpreter) return std::string("Unet::loadModel(): createFromFile() failed");
    MNN::ScheduleConfig conf;
    mSession = mInterpreter->createSession(conf);
    if (!mSession) return std::string("Unet::loadModel(): createSession() failed");

    // Verify input
    auto inputs = mInterpreter->getSessionInputAll(mSession);
    if (inputs.size() != 1) return std::string("Unet::loadModel(): invalid input number");
    auto input = inputs.begin()->second;
    if (input->getDimensionType() != MNN::Tensor::CAFFE) return std::string("Unet::loadModel(): invalid input dimension type");
    auto inputDims = input->shape();
    if (
            inputDims.size() != 4
            ||
            inputDims.at(0) != 1
            ||
            inputDims.at(1) != 3
            ||
            inputDims.at(2) <= 0
            ||
            inputDims.at(3) <= 0
            )
        return std::string("Unet::loadModel(): invalid input dimension");

    // Verify output
    auto outputs = mInterpreter->getSessionOutputAll(mSession);
    if (outputs.size() != 1) return std::string("Unet::loadModel(): invalid output number");
    auto output = outputs.begin()->second;
    if (output->getDimensionType() != MNN::Tensor::CAFFE) return std::string("Unet::loadModel(): invalid output dimension type");
    auto outputDims = output->shape();
    if (
            outputDims.size() != 4
            ||
            outputDims.at(0) != 1
            ||
            outputDims.at(1) != 1
            ||
            outputDims.at(2) != inputDims.at(2)
            ||
            outputDims.at(3) != inputDims.at(3)
            )
        return std::string("Unet::loadModel(): invalid output dimension");

    // Init params
    mHeight = inputDims.at(2);
    mWidth = inputDims.at(3);

    return std::string();
}

std::string Unet::preprocess(const uint8_t *image, int width, int height)
{
    if (width <= 0 || height <= 0 || !image)
        return std::string("Unet::preprocess(): invalid input");
    MNN::CV::Matrix matrix;
    matrix.setIdentity();
    matrix.postScale(1.0f * mWidth / width, 1.0f * mHeight / height);
    matrix.invert(&matrix);
    mPreprocessor->setMatrix(matrix);
    mPreprocessor->convert(image, width, height, 0, mInputTensors.begin()->second);
    return std::string();
}

std::string Unet::postprocess(int width, int height, std::vector<float> &result)
{
    float *ptr = mOutputTensors.begin()->second->host<float>();
    cv::Mat mask(mHeight, mWidth, CV_32FC1, ptr);
    mask.convertTo(mask, CV_8UC1);
    std::vector<cv::Vec4i> intLines;
    int lengthThresh = std::min(mWidth, mHeight) / 4;
    cv::HoughLinesP(mask, intLines, 1.0, 1.0 * CV_PI / 180, lengthThresh, lengthThresh, lengthThresh);

    result.push_back(intLines.size());
    for (int i = 0; i < intLines.size(); i++) {
        float angle = 180.0f * atan2(
                float(intLines[i][1] - intLines[i][3]) / mHeight * height,
                float(intLines[i][0] - intLines[i][2]) / mWidth * width
        ) / CV_PI;
        while (angle > 90.0f) angle -= 180.0f;
        while (angle < -90.0f) angle += 180.0f;
        result.push_back(angle);
        result.push_back(1.0 * intLines[i][0] / mWidth);
        result.push_back(1.0 * intLines[i][1] / mHeight);
        result.push_back(1.0 * intLines[i][2] / mWidth);
        result.push_back(1.0 * intLines[i][3] / mHeight);
    }
    return std::string();
}

std::string Unet::simulate(std::vector<float> &result)
{
    result.push_back(1);
    result.push_back(0);
    result.push_back(0);
    result.push_back(0.5);
    result.push_back(1);
    result.push_back(0.5);
    return std::string();
}

}
