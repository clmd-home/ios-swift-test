#include "basemodel.hpp"

extern "C" {

BaseModel::BaseModel() :
    mEnabled(true), mInitialized(false), mInterpreter(nullptr), mSession(nullptr), mPreprocessor(nullptr),
    mWidth(0), mHeight(0)
{
    mInputTensors.clear();
    mOutputTensors.clear();
}

BaseModel::~BaseModel()
{
    if (mSession) mInterpreter->releaseSession(mSession);
    if (mInterpreter) delete mInterpreter;
    if (mPreprocessor) delete mPreprocessor;
    for (auto it = mInputTensors.begin(); it != mInputTensors.end(); it++)
        delete it->second;
    for (auto it = mOutputTensors.begin(); it != mOutputTensors.end(); it++)
        delete it->second;
}

void BaseModel::initPreprocessor()
{
    MNN::CV::ImageProcess::Config config;
    config.filterType = MNN::CV::BILINEAR;
    config.wrap = MNN::CV::ZERO;
    config.sourceFormat = MNN::CV::RGBA;
    config.destFormat = MNN::CV::RGB;
    for (int i = 0; i < 4; i++) {
        config.mean[i] = 0.0f;
        config.normal[i] = 1.0f / 255.0f;
    }
    mPreprocessor = MNN::CV::ImageProcess::create(config);
}

std::string BaseModel::loadModel(const std::string &modelPath)
{
    return std::string("BaseModel::loadModel() Not Implemented!!!");
}

std::string BaseModel::loadConfig(const std::string &configPath)
{
    return std::string("BaseModel::loadConfig() Not Implemented!!!");
}

std::string BaseModel::preprocess(const uint8_t *image, int width, int height)
{
    return std::string("BaseModel::preprocess() Not Implemented!!!");
}

std::string BaseModel::postprocess(int width, int height, std::vector<float> &result)
{
    return std::string("BaseModel::postprocess() Not Implemented!!!");
}

std::string BaseModel::simulate(std::vector<float> &result)
{
    return std::string("BaseModel::simulate() Not Implemented!!!");
}

std::string BaseModel::init(const std::string &modelPath, const std::string &configPath)
{
    if (modelPath.empty()) {
        mEnabled = false;
        return std::string();
    }

    mEnabled = true;
    if (mInitialized) return std::string();
    initPreprocessor();
    std::string error = loadConfig(configPath);
    if (!error.empty()) return error;

    error = loadModel(modelPath);
    if (!error.empty()) return error;

    auto inputs = mInterpreter->getSessionInputAll(mSession);
    for (auto it = inputs.begin(); it != inputs.end(); it++)
        mInputTensors[it->first] = new MNN::Tensor(it->second, it->second->getDimensionType());

    auto outputs = mInterpreter->getSessionOutputAll(mSession);
    for (auto it = outputs.begin(); it != outputs.end(); it++)
        mOutputTensors[it->first] = new MNN::Tensor(it->second, it->second->getDimensionType());
    mInitialized = true;
    return std::string();
}

std::string BaseModel::infer(uint8_t *image, int width, int height, std::vector<float> &result)
{
    if (!mEnabled) {
        return simulate(result);
    }
    std::string error = preprocess(image, width, height);
    if (!error.empty()) return error;

    for (auto it = mInputTensors.begin(); it != mInputTensors.end(); it++)
        mInterpreter->getSessionInput(mSession, it->first.c_str())->copyFromHostTensor(it->second);

    MNN::ErrorCode errorCode = mInterpreter->runSession(mSession);
    if (errorCode != 0)
        return std::string("BaseModel::runSession() returned ") + std::to_string(errorCode);

    for (auto it = mOutputTensors.begin(); it != mOutputTensors.end(); it++)
        mInterpreter->getSessionOutput(mSession, it->first.c_str())->copyToHostTensor(it->second);

    return postprocess(width, height, result);
}

}
