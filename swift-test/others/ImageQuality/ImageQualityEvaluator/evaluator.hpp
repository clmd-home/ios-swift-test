#ifndef __EVALUATOR_H__
#define __EVALUATOR_H__

#include <vector>

extern "C" {

class Evaluator
{
public:
    enum class ImageQuality : int {
        IMAGE_QUALITY_OK = 0,
        IMAGE_QUALITY_LOW_RATIO = 1,
        IMAGE_QUALITY_LARGE_ANGLE = 2
    };
    static void evaluate(std::vector<float> &info, float angleThresh, float fgdRatioThresh);
};

}

#endif
