#ifndef __STITCHER_H__
#define __STITCHER_H__

#include <fstream>
#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/features2d.hpp>

extern "C" {

class Stitcher
{
private:
    int mInputWidth;
    int mInputHeight;
    int mActualWidth;
    int mActualHeight;
    cv::Mat mInputFrame;
    cv::Mat mActualFrame;

    cv::Ptr<cv::ORB> mFeatureDetector;
    cv::Ptr<cv::BFMatcher> mMatcher;
    std::vector<cv::Mat> mFrameQueue;
    std::vector<std::vector<cv::KeyPoint>> mKpQueue;
    std::vector<cv::Mat> mFeatDescQueue;

public:
    Stitcher();
    ~Stitcher();

    void preprocess(const uint8_t *image);
    std::string start(const uint8_t *image);
    std::string stitch(const uint8_t *image, std::vector<float> &result);
};
}

#endif
