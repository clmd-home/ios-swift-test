#include "yolov5.hpp"

extern "C" {

std::string YOLOv5::loadConfig(const std::string &configPath)
{
    std::string parseError;
    std::ifstream ifs(configPath);
    std::string content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
    json11::Json config = json11::Json::parse(content, parseError);
    if (!parseError.empty()) return std::string("YOLOv5::loadConfig(): ") + parseError;

    mIouThresh = config["iou_thresh"].number_value();
    if (mIouThresh <= 0.0f || mIouThresh >= 1.0f)
        return std::string("YOLOv5::loadConfig(): invalid IoU thresh ") + std::to_string(mIouThresh);

    mIofThresh = config["iof_thresh"].number_value();
    if (mIofThresh <= 0.0f || mIofThresh >= 1.0f)
        return std::string("YOLOv5::loadConfig(): invalid IoF thresh ") + std::to_string(mIofThresh);

    mConfThresh = config["conf_thresh"].number_value();
    if (mConfThresh <= 0.0f || mConfThresh >= 1.0f)
        return std::string("YOLOv5::loadConfig(): invalid conf thresh ") + std::to_string(mConfThresh);

    json11::Json::array jAnchors = config["anchors"].array_items();
    if (jAnchors.size() != 3)
        return std::string("YOLOv5::loadConfig(): invalid stride number ") + std::to_string(jAnchors.size());

    for (int i = 0; i < 3; i++) {
        int stride = 8 * pow(2, i);
        json11::Json::array jAnchorsPerStride = jAnchors[i].array_items();
        if (jAnchorsPerStride.size() != 3)
            return std::string("YOLOv5::loadConfig(): invalid anchor number per stride ") + std::to_string(jAnchorsPerStride.size());
        mAnchors[stride].clear();
        for (int j = 0; j < 3; j++) {
            json11::Json::array jAnchorsPerDetector = jAnchorsPerStride[j].array_items();
            if (jAnchorsPerDetector.size() != 2)
                return std::string("YOLOv5::loadConfig(): invalid anchor size ") + std::to_string(jAnchorsPerDetector.size());
            mAnchors[stride].push_back(std::vector<float>());
            for (int k = 0; k < 2; k++)
                mAnchors[stride].back().push_back(jAnchorsPerDetector[k].number_value());
        }
    }

    return std::string();
}

std::string YOLOv5::loadModel(const std::string &modelPath)
{
    // Load model
    mInterpreter = MNN::Interpreter::createFromFile(modelPath.c_str());
    if (!mInterpreter) return std::string("YOLOv5::loadModel(): createFromFile() failed");
    MNN::ScheduleConfig conf;
    mSession = mInterpreter->createSession(conf);
    if (!mSession) return std::string("YOLOv5::loadModel(): createSession() failed");

    // Verify input
    auto inputs = mInterpreter->getSessionInputAll(mSession);
    if (inputs.size() != 1) return std::string("YOLOv5::loadModel(): invalid input number");
    auto input = inputs.begin()->second;
    if (input->getDimensionType() != MNN::Tensor::CAFFE) return std::string("YOLOv5::loadModel(): invalid input dimension type");
    auto inputDims = input->shape();
    if (
            inputDims.size() != 4
            ||
            inputDims.at(0) != 1
            ||
            inputDims.at(1) != 3
            ||
            inputDims.at(2) <= 0
            ||
            inputDims.at(3) <= 0
            )
        return std::string("YOLOv5::loadModel(): invalid input dimension");

    // Verify output
    auto outputs = mInterpreter->getSessionOutputAll(mSession);
    if (outputs.size() != 3) return std::string("YOLOv5::loadModel(): invalid output number");
    for (auto it = outputs.begin(); it != outputs.end(); it++) {
        const MNN::Tensor *output = it->second;
        if (output->getDimensionType() != MNN::Tensor::CAFFE) return std::string("YOLOv5::loadModel(): invalid output dimension type");
        auto outputDims = output->shape();
        if (
                outputDims.size() != 5
                ||
                outputDims.at(0) != 1
                ||
                outputDims.at(1) != 3
                ||
                inputDims.at(2) / outputDims.at(2) != inputDims.at(3) / outputDims.at(3)
                ||
                outputDims.at(4) < 6
                )
            return std::string("YOLOv5::loadModel(): invalid output dimension");
    }

    // Init params
    mHeight = inputDims.at(2);
    mWidth = inputDims.at(3);

    return std::string();
}

float YOLOv5::sigmoid(float x) {
    if (x > 0) return 1.0f / (1.0f + std::exp(-x));
    else return std::exp(x) / (std::exp(x) + 1.0f);
}

bool YOLOv5::convertAndVerifyBox(Object &obj) {
    float l = std::max(0.0f, obj.l() - mDx);
    float r = std::min(mDw, obj.r() - mDx);
    float t = std::max(0.0f, obj.t() - mDy);
    float b = std::min(mDh, obj.b() - mDy);
    if (l >= r || t >= b) return false;
    obj.fromLTRB(l / mDw, t / mDh, r / mDw, b / mDh);
    return true;
}

std::string YOLOv5::preprocess(const uint8_t *image, int width, int height)
{
    if (width <= 0 || height <= 0 || !image)
        return std::string("YOLOv5::preprocess(): invalid input");
    float ratio = std::min(1.0f * mWidth / width, 1.0f * mHeight / height);
    mDw = width * ratio;
    mDx = 0.5 * (mWidth - mDw);
    mDh = height * ratio;
    mDy = 0.5 * (mHeight - mDh);
    MNN::CV::Matrix matrix;
    matrix.setIdentity();
    matrix.postScale(ratio, ratio);
    matrix.postTranslate(mDx, mDy);
    matrix.invert(&matrix);
    mPreprocessor->setMatrix(matrix);
    mPreprocessor->convert(image, width, height, 0, mInputTensors.begin()->second);
    return std::string();
}

void YOLOv5::postprocessPerStride(const MNN::Tensor *feature, std::vector<Object> &objects) {
    std::vector<int> dims = feature->shape();
    int H = dims[2];
    int W = dims[3];
    int C = dims[4];
    int stride = mHeight / H;

    // Parse objects
    float *ptr = feature->host<float>();
    Object obj;
    for (int d = 0; d < 3; d++) {
        float anchorW = mAnchors[stride][d][0];
        float anchorH = mAnchors[stride][d][1];
        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                int i = d * H * W * C + y * W * C + x * C;
                obj.conf = -1.0f;
                // Sigmoid
                for (int c = 0; c < C; c++) {
                    ptr[i + c] = sigmoid(ptr[i + c]);
                }
                // Parse confidence
                for (int c = 5; c < C; c++) {
                    float conf = ptr[i + c] * ptr[i + 4];
                    if (conf <= mConfThresh || conf <= obj.conf) continue;
                    obj.conf = conf;
                    obj.id = c - 5;
                }
                if (obj.conf <= 0.0f) continue;
                // Parse center
                obj.x = (ptr[i + 0] * 2.0f - 0.5f + x) * stride;
                obj.y = (ptr[i + 1] * 2.0f - 0.5f + y) * stride;
                // Parse size
                obj.w = std::pow(ptr[i + 2] * 2.0f, 2.0f) * anchorW;
                obj.h = std::pow(ptr[i + 3] * 2.0f, 2.0f) * anchorH;
                if (convertAndVerifyBox(obj))
                    objects.push_back(obj);
            }
        }
    }
}

std::string YOLOv5::postprocess(int width, int height, std::vector<float> &result)
{
    mObjectBeforeNms.clear();
    mObjectsAfterNms.clear();

    for (auto it = mOutputTensors.begin(); it != mOutputTensors.end(); it++)
        postprocessPerStride(it->second, mObjectBeforeNms);
    std::sort(mObjectBeforeNms.begin(), mObjectBeforeNms.end(), Object::compare);

    for (int i = 0; i < mObjectBeforeNms.size(); i++) {
        if (mObjectBeforeNms[i].conf <= 0.0f) continue;
        mObjectsAfterNms.push_back(mObjectBeforeNms[i]);
        for (int j = i + 1; j < mObjectBeforeNms.size(); j++)
            if (mObjectBeforeNms[i].calcIoU(mObjectBeforeNms[j]) > mIouThresh || mObjectBeforeNms[i].calcIoF(mObjectBeforeNms[j]) > mIofThresh)
                mObjectBeforeNms[j].conf = -1.0f;
    }

    result.push_back(mObjectsAfterNms.size());
    for (const Object &obj : mObjectsAfterNms) {
        result.push_back(obj.conf);
        result.push_back(obj.l());
        result.push_back(obj.t());
        result.push_back(obj.r());
        result.push_back(obj.b());
    }
    return std::string();
}

std::string YOLOv5::simulate(std::vector<float> &result)
{
    result.push_back(1);
    result.push_back(1);
    result.push_back(0);
    result.push_back(0);
    result.push_back(1);
    result.push_back(1);
    return std::string();
}

}
