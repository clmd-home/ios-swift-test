#ifndef __YOLOV5_H__
#define __YOLOV5_H__

#include "basemodel.hpp"

extern "C" {

class Object {
public:
    float x;
    float y;
    float w;
    float h;
    float conf;
    int id;

    Object () {}
    Object (float x, float y, float w, float h, float conf, int id) : x(x), y(y), w(w), h(h), conf(conf), id(id) {}
    void fromLTRB (float l, float t, float r, float b) {
        x = 0.5 * (r + l);
        y = 0.5 * (b + t);
        w = r - l;
        h = b - t;
    }
    float l () const { return x - 0.5 * w; }
    float r () const  { return x + 0.5 * w; }
    float t () const { return y - 0.5 * h; }
    float b () const  { return y + 0.5 * h; }
    float area () const { return w * h; };
    float calcIoU (const Object &obj) {
        float il = std::max(obj.l(), l());
        float it = std::max(obj.t(), t());
        float ir = std::min(obj.r(), r());
        float ib = std::min(obj.b(), b());

        if (il >= ir || it >= ib) return 0.0f;
        float its = (ir - il) * (ib - it);
        float uni = obj.area() + area() - its;
        return its / uni;
    }
    float calcIoF (const Object &obj) {
        float il = std::max(obj.l(), l());
        float it = std::max(obj.t(), t());
        float ir = std::min(obj.r(), r());
        float ib = std::min(obj.b(), b());

        if (il >= ir || it >= ib) return 0.0f;
        float its = (ir - il) * (ib - it);
        float uni = std::min(area(), obj.area());
        return its / uni;
    }
    static bool compare (const Object &a, const Object &b) {
        return a.conf > b.conf;
    }
    static bool left (const Object &a, const Object &b) {
        return a.l() < b.l();
    }
};

class YOLOv5 : public BaseModel
{
private:
    float mIouThresh;
    float mIofThresh;
    float mConfThresh;
    std::map<int, std::vector<std::vector<float>>> mAnchors;
    std::vector<Object> mObjectBeforeNms, mObjectsAfterNms;

    float mDx;
    float mDy;
    float mDw;
    float mDh;

    static float sigmoid(float x);
    bool convertAndVerifyBox(Object &obj);
    void postprocessPerStride(const MNN::Tensor *feature, std::vector<Object> &objects);

protected:
    virtual std::string loadModel(const std::string &modelPath);
    virtual std::string loadConfig(const std::string &configPath);
    virtual std::string preprocess(const uint8_t *image, int width, int height);
    virtual std::string postprocess(int width, int height, std::vector<float> &result);
    virtual std::string simulate(std::vector<float> &result);
};
}

#endif
