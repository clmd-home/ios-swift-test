//
//  AppDelegate.swift
//  swift-test
//
//  Created by clmd on 2020/5/13.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit
import Bugly

@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        CatchCrash.share().installUncaughtExceptionHandler()
//        Bugly.start(withAppId: "9cff4d6525")
        //crash捕获
        crashHandle { (crashInfoArr) in
            print(crashInfoArr.count)
            var content = ""
            for info in crashInfoArr{
                //将上一次崩溃信息显示在屏幕上
                content = info + "\n\n <<<<<<<<<<<<<<< \n\n" + content
            }
            DispatchQueue.main.async {
                let contentV = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
                let infoLabel = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - 30))
                infoLabel.backgroundColor = .gray
                infoLabel.textColor = .white
                infoLabel.isEditable = false
                infoLabel.text = content
                contentV.addSubview(infoLabel)
                
                let btn = UIButton()
                btn.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
                btn.setTitle("close", for: .normal)
                btn.addTarget(self, action: #selector(self.hideView(sender:)), for: .touchUpInside)
                contentV.addSubview(btn)
                btn.snp.makeConstraints { (make) in
                    make.bottom.left.right.equalToSuperview()
                    make.height.equalTo(30)
                }
                
                UIApplication.shared.keyWindow?.addSubview(contentV)
            }
            return true
        }
        
        UMConfigure.initWithAppkey("6036034e425ec25f100249da", channel: "test")
        
        return true
    }
    @objc func hideView(sender: UIButton){
        if let superV = sender.superview {
            superV.removeFromSuperview()
        }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

