//
//  TaskListViewController.swift
//  swift-test
//
//  Created by clmd on 2020/11/10.
//  Copyright © 2020 clmd铭. All rights reserved.
//

import UIKit

class TaskListViewController: UIViewController {

    @IBOutlet weak var undoneView: UIView!
    @IBOutlet weak var taskNumLB: UILabel!
    @IBOutlet weak var taskListTBV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        taskListTBV.register(UINib(nibName: "TaskCareTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskCareTableViewCellID")
        taskListTBV.register(UINib(nibName: "TaskTableViewCell", bundle: nil), forCellReuseIdentifier: "TaskTableViewCellID")
        taskListTBV.addShadow()
        
        let tv = typeOptionView()
        tv.titleText = "未完成"
        tv.num = 3
        tv.setStyle(isNormal: true)
        undoneView.addSubview(tv)
        tv.snp.makeConstraints({$0.edges.equalToSuperview()})
        // Do any additional setup after loading the view.
    }
    
    func showTask() {
        let stv = SenceTaskView.sharedInstance()
        stv.selectIndex = {index in
            
            
        }
        self.view.addSubview(stv)
        stv.snp.makeConstraints({$0.edges.equalToSuperview()})
    }

}

extension TaskListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 64 : 48
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "TaskCareTableViewCellID")
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else{
            if let tcell = tableView.dequeueReusableCell(withIdentifier: "TaskTableViewCellID") as? TaskTableViewCell {
                tcell.lodingImgV.isHidden = false
                tcell.statuLB.isHidden = true
                tcell.loding()
                cell = tcell
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
        showTask()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return [deleteAction(), deleteAction()]
    }
    
    private func deleteAction() -> UITableViewRowAction {
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { (action, index) in
            
        }
        delete.backgroundColor = UIColor(hex:"fe5d5c")
        return delete
    }
}
