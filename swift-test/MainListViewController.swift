//
//  MainListViewController.swift
//  swift-test
//
//  Created by clmd on 2021/2/1.
//  Copyright © 2021 clmd铭. All rights reserved.
//

import UIKit

struct Page {
    var title: String = ""
    var vc: UIViewController
}

class MainListViewController: UIViewController {

    @IBOutlet weak var mainListTBV: UITableView!
    
    private var dataSource: [Page] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource = [
            Page(title: "Test Page", vc: getVC(withName: "Main", vcId: "TestViewController")),
            Page(title: "横滑的可点击按钮", vc: TTwoViewController()),
            Page(title: "Model recognition", vc: getVC(withName: "Main", vcId: "ModelRecognitionVC")),
            Page(title: "写入cookie的webview", vc: WebCookieViewController()),
            Page(title: "选择题", vc: ComponentViewController())
        ]
        mainListTBV.reloadData()
    }
}

extension MainListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = dataSource[indexPath.row].title
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        MobClick.event("pushVC", label: dataSource[indexPath.row].title)
        
        let vc = dataSource[indexPath.row].vc
        vc.title = dataSource[indexPath.row].title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
